var Notificaciones = function () {
  var self = this;

  this.bloque =
    "<div id='notificaciones'>" +
    "<div id='triangulo-notificaciones'></div>" +
    "<div class='row '>" +
    "<div class='col-lg-12' id='cabecarea-notificaciones'>Notificaciones</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='listaNotificaciones'></div>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.insertarNotificaciones = function (itemsNotificaciones) {
    $.each(itemsNotificaciones, function (index, item) {
      $("#listaNotificaciones").append(item.bloque);
    });

    $("#zonaNotificacion").html("<div id='numero-notificaciones' class='contenido-centrado'>" + itemsNotificaciones.length + "</div>");
  }
  this.acomodarBloque = function () {
    var posicionNotificaciones = $("#zonaNotificacion").offset();
    $("#notificaciones").offset({
      top: posicionNotificaciones.top + 32,
      left: posicionNotificaciones.left - 170
    });
  }

  this
}
