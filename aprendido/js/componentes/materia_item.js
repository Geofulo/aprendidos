var MateriaItem = function (nombre, grado, idMateria, isEliminable) {
  this.nombre = nombre;
  this.grado = grado;

  this.bloque = "<div class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='materia_item'>" +
    "<div class='row'>" +
    "<div class='col-lg-10 nombre' >" +
    "<strong>" + this.nombre + "</strong>" +
    "</div>" +
    "<div class='col-lg-1'>" +
    ((isEliminable) ?
      "<div class='eliminarMateria' id='eliminar_" + idMateria + "'>x</div>" :
      '') +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-11 " + ((!isEliminable) ? "col-lg-offset-1" : "") + " grado'>" +
    this.grado +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
};
