var NotificacionesItem = function (idNotificacion, usuario, mensaje, tiempo, ultima) {
  this.bloque = "<div class='row' id='notificacion_" + idNotificacion + "'>" +
    "<div class='col-lg-12 notificacionesItem " + ((ultima) ? "ultima-notificacion" : "notificacion-intermedia") + "'>" +
    "<div class='foto-notificacion' id='foto-notificacion-" + idNotificacion + "'></div>" +
    "<div class='usuario-notificacion' id='usuario-notificacion-" + idNotificacion + "'>" + usuario + "</div>" +
    "<div class='mensaje-notificacion' id='mensaje-notificacion-" + idNotificacion + "'>" + mensaje + "</div>" +
    "<div class='tiempo-notificacion contenido-derecha' id='tiempo-notificacion-" + idNotificacion + "'>" + tiempo + "</div>" +
    "</div>" +
    "</div>";

  this.activarEventos = function () {
    $("#notificacion_" + idNotificacion).click(function (event) {
      console.log(event);
      var idNotificacion = (event.currentTarget.id).split("_")[1];
      alert(idNotificacion);
      everlive.data('Clase').updateSingle({
        'Id': idNotificacion,
        'isNotificacionPendiente': false,
      })
    });
  }
}
