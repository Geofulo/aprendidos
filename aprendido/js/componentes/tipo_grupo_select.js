var TipoGrupoSelect = function (titulo) {
  this.titulo = titulo;
  var self = this;
  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<label>" + this.titulo + "</label>" +
    "</div>" +
    "<div class='col-lg-6' contenido-derecha>" +
    "<span id='mostrarClasesIndividualesSelect'></span>" +
    "<span id='mostrarClasesSeparacionSelect'></span>" +
    "<span id='mostrarClasesGrupalesSelect'></span>" +
    "</div>" +
    "</div>" +
    "<input type='hidden' value='0' id='claseGrupalField'>" +
    "<input type='hidden' value='0' id='claseIndividualField'>" +

    "<div class='area-opciones'>" +
    "<div id='opcion-clases-individuales' class='individualDesactivado'></div>" +
    "<div id='opcion-clases-grupales' class='grupalDesactivado derecha'></div>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.activarClasesIndividuales = function () {
    opcion = $("#opcion-clases-individuales");
    separacion = $("#mostrarClasesSeparacionSelect");
    descripcion = $("#mostrarClasesIndividualesSelect");
    contraria = $("#mostrarClasesGrupalesSelect").text();

    $("#claseIndividualField").val('1');
    opcion.removeClass('individualDesactivado').removeClass('individualActivado').addClass('individualActivado');
    descripcion.text('individuales');
    if (contraria !== '') {
      separacion.text(', ');
    }
  };

  this.desactivarClasesIndividuales = function () {
    opcion = $("#opcion-clases-individuales");
    separacion = $("#mostrarClasesSeparacionSelect");
    descripcion = $("#mostrarClasesIndividualesSelect");
    contraria = $("#mostrarClasesGrupalesSelect").text();

    $("#claseIndividualField").val('0');
    opcion.removeClass('individualDesactivado').removeClass('individualActivado').addClass('individualDesactivado');
    descripcion.text('');
    separacion.text('');
  }

  this.activarClasesGrupales = function () {
    opcion = $("#opcion-clases-grupales");
    separacion = $("#mostrarClasesSeparacionSelect");
    descripcion = $("#mostrarClasesGrupalesSelect");
    contraria = $("#mostrarClasesIndividualesSelect").text();

    $("#claseGrupalField").val('1');
    opcion.removeClass('grupalDesactivado').addClass('grupalActivado');
    descripcion.text('grupales');
    if (contraria !== '') {
      separacion.text(', ');
    }
  };

  this.desactivarClasesGrupales = function () {
    opcion = $("#opcion-clases-grupales");
    separacion = $("#mostrarClasesSeparacionSelect");
    descripcion = $("#mostrarClasesGrupalesSelect");
    contraria = $("#mostrarClasesIndividualesSelect").text();

    $("#claseGrupalField").val('0');
    opcion.removeClass('grupalDesactivado').addClass('grupalDesactivado');
    descripcion.text('');
    separacion.text('');

  };

  this.isGrupal = function () {
    if ($("#claseGrupalField").val() == '1')
      return true;
    else
      return false;
  }

  this.isIndividual = function () {
    if ($("#claseIndividualField").val() == '1')
      return true;
    else
      return false;
  }

  this.activarEventos = function () {
    $("#opcion-clases-grupales").click(function () {
      if (self.isGrupal()) {
        self.desactivarClasesGrupales();
      } else {
        self.activarClasesGrupales();
      }

      $("#opcion-clases-grupales").trigger('grupalesChange');
    });

    $("#opcion-clases-individuales").click(function () {
      if (self.isIndividual()) {
        self.desactivarClasesIndividuales();
      } else {
        self.activarClasesIndividuales();
      }

      $("#opcion-clases-individuales").trigger('individualesChange');
    });
  }

  this.getInidividual = function () {
    return $("#opcion-clases-individuales");
  }

  this.getGrupal = function () {
    return $("#opcion-clases-grupales");
  }

  this.activarFiltrado = function () {
    $("#opcion-clases-individuales").on('individualesChange', function () {
      filtro.ClasesPersonal = self.isIndividual();
      if (!limpiar)
        filtrar(1);
    });

    $("#opcion-clases-grupales").on('grupalesChange', function () {
      filtro.ClasesGrupo = self.isGrupal();
      if (!limpiar)
        filtrar(1);
    });
  }
}
