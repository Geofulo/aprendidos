var HorarioSelect = function () {
  var self = this;

  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<label>Horarios</label>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<input type='hidden' value='0' id='horarioDiaField'>" +
    "<input type='hidden' value='0' id='horarioTardeField'>" +
    "<input type='hidden' value='0' id='horarioNocheField'>" +
    "<span id='mostrarHorarioDia'></span> " + "<span id='mostrarHoarioSeparacion1'></span>" +
    "<span id='mostrarHorarioTarde'></span> " + "<span id='mostrarHoarioSeparacion2'></span>" +
    "<span id='mostrarHorarioNoche'></span> " +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='area-opciones contenido-centrado'>" +
    "<div id='opcion-horario-dia' class='horario-dia-desactivado izquierda icono'></div>" +
    "<div id='opcion-horario-tarde' class='horario-tarde-desactivado centro icono'></div>" +
    "<div id='opcion-horario-noche' class='horario-noche-desactivado derecha icono'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.activarHorarioDia = function () {
    opcion = $("#opcion-horario-dia");
    separacion1 = $("#mostrarHoarioSeparacion1");
    separacion2 = $("#mostrarHoarioSeparacion2");
    textoTarde = $("#mostrarHorarioTarde").text();
    textoNoche = $("#mostrarHorarioNoche").text();

    opcion.removeClass('horario-dia-desactivado').addClass('horario-dia-activado');
    $("#mostrarHorarioDia").text('Dia');
    $("#horarioDiaField").val('1');
    if (textoTarde !== '' || textoNoche !== '') {
      separacion1.text(', ');
    } else {
      separacion1.text('');
    }


  };

  this.desactivarHorarioDia = function () {
    opcion = $("#opcion-horario-dia");
    separacion1 = $("#mostrarHoarioSeparacion1");
    separacion2 = $("#mostrarHoarioSeparacion2");
    textoTarde = $("#mostrarHorarioTarde").text();
    textoNoche = $("#mostrarHorarioNoche").text();

    opcion.removeClass('horario-dia-activado').addClass('horario-dia-desactivado');
    $("#mostrarHorarioDia").text('');
    $("#horarioDiaField").val('0');
    separacion1.text('');
  }

  this.activarHorarioTarde = function () {
    opcion = $("#opcion-horario-tarde");

    separacion1 = $("#mostrarHoarioSeparacion1");
    separacion2 = $("#mostrarHoarioSeparacion2");
    textoDia = $("#mostrarHorarioDia").text();
    textoNoche = $("#mostrarHorarioNoche").text();

    opcion.removeClass('horario-tarde-desactivado').addClass('horario-tarde-activado');
    $("#mostrarHorarioTarde").text('Tarde');
    $("#horarioTardeField").val('1');
    if (textoDia !== '') {
      separacion1.text(', ')
    }
    if (textoNoche !== '') {
      separacion2.text(', ');
    }
  };

  this.desactivarHorarioTarde = function () {
    opcion = $("#opcion-horario-tarde");
    separacion1 = $("#mostrarHoarioSeparacion1");
    separacion2 = $("#mostrarHoarioSeparacion2");
    textoDia = $("#mostrarHorarioDia").text();
    textoNoche = $("#mostrarHorarioNoche").text();

    opcion.removeClass('horario-tarde-activado').addClass('horario-tarde-desactivado');
    $("#mostrarHorarioTarde").text('');
    $("#horarioTardeField").val('0');
    separacion2.text('');
    if (textoNoche === '') {
      separacion1.text('');
    }
  }

  this.activarHorarioNoche = function () {
    opcion = $("#opcion-horario-noche");
    separacion1 = $("#mostrarHoarioSeparacion1");
    separacion2 = $("#mostrarHoarioSeparacion2");
    textoDia = $("#mostrarHorarioDia").text();
    textoTarde = $("#mostrarHorarioTarde").text();

    opcion.removeClass('horario-noche-desactivado').addClass('horario-noche-activado');
    $("#mostrarHorarioNoche").text('Noche');
    $("#horarioNocheField").val('1');
    if (textoDia !== '' || textoTarde !== '') {
      separacion2.text(', ');
    }
  };

  this.desactivarHorarioNoche = function () {
    opcion = $("#opcion-horario-noche");
    separacion1 = $("#mostrarHoarioSeparacion1");
    separacion2 = $("#mostrarHoarioSeparacion2");
    textoDia = $("#mostrarHorarioDia").text();
    textoTarde = $("#mostrarHorarioTarde").text();

    opcion.removeClass('horario-noche-activado').addClass('horario-noche-desactivado');
    $("#mostrarHorarioNoche").text('');
    $("#horarioNocheField").val('0');
    separacion2.text('');
    if (textoTarde === '') {
      separacion1.text('');
    }
  }

  this.isHorarioDia = function () {
    if ($("#horarioDiaField").val() == '1')
      return true;
    else
      return false;
  }

  this.isHorarioTarde = function () {
    if ($("#horarioTardeField").val() == '1')
      return true;
    else
      return false;
  }

  this.isHorarioNoche = function () {
    if ($("#horarioNocheField").val() == '1')
      return true;
    else
      return false;
  }

  this.activarEventos = function () {
    $("#opcion-horario-dia").click(function () {
      if (self.isHorarioDia())
        self.desactivarHorarioDia();
      else
        self.activarHorarioDia();

      $("#opcion-horario-dia").trigger('horarioDiaChange');
    });

    $("#opcion-horario-tarde").click(function () {
      if (self.isHorarioTarde())
        self.desactivarHorarioTarde();
      else
        self.activarHorarioTarde();
      $("#opcion-horario-tarde").trigger('horarioTardeChange');
    });


    $("#opcion-horario-noche").click(function () {
      if (self.isHorarioNoche())
        self.desactivarHorarioNoche();
      else
        self.activarHorarioNoche();
      $("#opcion-horario-noche").trigger('horarioNocheChange');
    });
  }

  this.getHorarioDia = function () {
    return $("#opcion-horario-dia");
  }

  this.getHorarioTarde = function () {
    return $("#opcion-horario-tarde");
  }

  this.getHorarioNoche = function () {
    return $("#opcion-horario-noche");
  }

  this.activarFiltrado = function () {
    $("#opcion-horario-noche").on('horarioNocheChange', function () {
      filtro.HorarioNoche = self.isHorarioNoche();
      if (!limpiar)
        filtrar(1);
    });

    $("#opcion-horario-tarde").on('horarioTardeChange', function () {
      filtro.HorarioTarde = self.isHorarioTarde();
      if (!limpiar)
        filtrar(1);
    });

    $("#opcion-horario-dia").on('horarioDiaChange', function () {
      filtro.HorarioDia = self.isHorarioDia();
      if (!limpiar)
        filtrar(1);
    });
  }
}
