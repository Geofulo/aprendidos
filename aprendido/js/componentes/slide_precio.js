var SlidePrecio = function (titulo, valorInferior, valorSuperior, incremento, valorInicial) {
  this.valorInferior = valorInferior;
  this.valorSuperior = valorSuperior;
  this.incremento = incremento;
  this.valorInicial = valorInicial;
  var self = this;
  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<label>" + titulo + "</label>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div id='mostrarPrecioActualSlider' class='derecha'>$" + this.valorInicial + "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-12'>" +
    "<input type='hidden' id='precioMaximoField'>" +
    "<div class='row area-opciones'>" +
    "<div class='col-lg-1'>" +
    "<div class='reduce-slider' id='reduce-precio'></div>" +
    "</div>" +
    "<div class='col-lg-9' >" +
    "<div>" +
    "<div class='slider-general' id='precio-slider'></div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-1'>" +
    "<div class='aumenta-slider' id='aumenta-precio'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.iniciar = function () {
    $("#precio-slider").slider({
      min: this.valorInferior,
      max: this.valorSuperior,
      step: this.incremento,
      animate: "fast",
      value: this.valorInicial,
      change: function (event, ui) {
        if (isNaN(ui.value))
          ui.value = self.valorInicial;

        $("#mostrarPrecioActualSlider").text("$ " + ui.value);
        $("#precioMaximoField").val(ui.value);
      }
    });
  };

  this.activarEventos = function () {
    $("#reduce-precio").click({
      valorInferior: this.valorInferior,
      incremento: this.incremento
    }, function () {
      precio_slider = $("#precio-slider");
      valorActual = precio_slider.slider("value");
      if (isNaN(valorActual))
        valorActual = 0;
      if (valorActual > (valorInferior - 1)) {
        precio_slider.slider("value", (valorActual - incremento));
      }
    });

    $("#aumenta-precio").click({
      valorSuperior: this.valorSuperior,
      incremento: this.incremento
    }, function () {
      precio_slider = $("#precio-slider");
      valorActual = precio_slider.slider("value");
      if (isNaN(valorActual))
        valorActual = 0;
      if (valorActual < (valorSuperior)) {
        precio_slider.slider("value", (valorActual + incremento));
      }
    });
  }

  this.setPrecio = function (precio) {
    precio_slider = $("#precio-slider");
    precio_slider.slider("value", precio);
  }

  this.getSlider = function () {
    return $("#precio-slider");
  }

  this.activarFiltrado = function () {

    $("#precio-slider").on('slidechange', function (event, ui) {
      if (ui.value == 0) {
        delete filtro.Precio;
      } else {
        filtro.Precio = ui.value;
      }

      if (!limpiar)
        filtrar(1);
    });
  }
}
