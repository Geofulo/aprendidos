var VaADomicilioSelect = function (activo) {
  this.activo = activo;
  var self = this;
  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<label>Va a domicilio</label>" +
    "</div>" +
    "<div class='col-lg-6 contenido-derecha'>" +
    "<div id='mostrarVaADomicilio'>" + ((activo) ? 'Sí' : 'No') + "</div>" +
    "</div>" +
    "</div>" +

    "<input type='hidden' id='vaADomicilioField'>" +

    "<div class='row area-opciones'>" +
    "<div class='col-lg-12'>" +
    "<span id='opcion-va-a-domicilio' class='icono " + ((activo) ? 'va-a-domicilio-activado' : 'va-a-domicilio-desactivado') + "'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
  this.activarEventos = function () {
    $("#opcion-va-a-domicilio").click(function () {
      opcion = $("#opcion-va-a-domicilio");
      if (opcion.attr('class').includes('va-a-domicilio-activado')) {
        opcion.removeClass('va-a-domicilio-activado').addClass('va-a-domicilio-desactivado');
        $("#vaADomicilioField").val('0');
        $("#mostrarVaADomicilio").text('No');
      } else if (opcion.attr('class').includes('va-a-domicilio-desactivado')) {
        opcion.removeClass('va-a-domicilio-desactivado').addClass('va-a-domicilio-activado');
        $("#vaADomicilioField").val('1');
        $("#mostrarVaADomicilio").text('Sí');
      }

      $("#opcion-va-a-domicilio").trigger('change');
    });
  }

  this.activarVaADomicilio = function () {
    $("#mostrarVaADomicilio").text('Sí');
    $("#vaADomicilioField").val('1');
    $("#opcion-va-a-domicilio").removeClass('va-a-domicilio-activado').removeClass('va-a-domicilio-desactivado').addClass('va-a-domicilio-activado');
  }

  this.desactivarVaADomicilio = function () {
    $("#mostrarVaADomicilio").text('No');
    $("#vaADomicilioField").val('0');
    $("#opcion-va-a-domicilio").removeClass('va-a-domicilio-activado').removeClass('va-a-domicilio-desactivado').addClass('va-a-domicilio-desactivado');
  }

  this.getSelect = function () {
    return $("#opcion-va-a-domicilio");
  }

  this.isVaADomicilio = function () {
    if ($("#vaADomicilioField").val() == '1')
      return true;
    else
      return false;
  }

  this.activarFiltrado = function () {
    $("#opcion-va-a-domicilio").on('change', function () {
      filtro.VaADomicilio = self.isVaADomicilio();
      if (!limpiar)
        filtrar(1);
    });
  }
}
