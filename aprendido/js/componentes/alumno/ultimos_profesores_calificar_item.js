var UltimosProfesoresCalificarItem = function (id_clase, foto, nombre, everlive, id_profesor) {
  this.nombre = nombre;
  this.foto = (foto) ?
    "<div class='foto' style='background-image: url(" + foto + "); background-size: cover; background-position: center'></div>" :
    "<div class='foto'></div>";

  this.codigoCalificacion = function () {
    var codigo = "";
    for (i = 1; i <= 5; i++) {
      codigo = codigo.concat('<span class="estrella-negativa-chica" id="calificacion_' + i + '_' + id_clase + '"></span>');
    }
    return codigo;
  };

  this.bloque =
    "<div class='row ultimos-profesores'>" +
    "<div class='col-lg-3'>" +
    this.foto +
    "</div>" +
    "<div class='col-lg-9'>" +
    "<div class='row'>" +
    "<div class='col-lg-8'>" +
    "<div class='nombre'>" +
    this.nombre +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div class='calificacion'>" +
    this.codigoCalificacion() +
    "<inpu type='hidden' id='calificacionField_" + id_clase + "'/>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<input type='text' id='comentarioField_" + id_clase + "' class='obscuro ajustable'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='error' id='error_" + id_clase + "'></div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-derecha'>" +
    "<a href='#' id='calificar_" + id_clase + "' class='enlace'>calificar</a>" +
    "</div>"
  "</div>" +
  "</div>" +
  "</div>";

  this.activarEventos = function () {
    for (var i = 1; i <= 5; i++) {
      $("#calificacion_" + i + "_" + id_clase).click(function (event) {
        $("#calificacionField_" + id_clase).val(event.currentTarget.id.split("_")[1]);
        for (var j = 1; j <= 5; j++) {
          $("#calificacion_" + j + "_" + id_clase).removeClass().addClass('estrella-negativa-chica');
          if (j <= $("#calificacionField_" + id_clase).val()) {
            $("#calificacion_" + j + "_" + id_clase).removeClass('estrella-negativa-chica').addClass('estrella-positiva-chica');
          }
        }
      });
    }

    $("#calificar_" + id_clase).click(function (event) {
      event.preventDefault();
      if ($("#calificacionField_" + id_clase).val() == '' || $("#comentarioField_" + id_clase).val() == '') {
        $("#error_" + id_clase).text('Poner puntaje y comentario');
      }
      everlive.data('Clase').updateSingle({
        Id: id_clase,
        Puntuacion: $("#calificacionField_" + id_clase).val(),
        Comentario: $("#comentarioField_" + id_clase).val()
      }, function (data) {
        everlive.data('Profesor').getById(id_profesor).then(
          function (data) {
            everlive.data('Profesor').updateSingle({
              Id: data.result.Id,
              Puntaje: ((data.result.Puntaje + $("#calificacionField_" + id_clase).val()) / 2)
            }, function (data) {
              window.location.reload();
            })
          });
      });
    });
  }
}
