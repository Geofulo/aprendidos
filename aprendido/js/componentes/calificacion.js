/*jslint plusplus: true*/
var Calificacion = function (calificacion) {
  "use strict";
  this.calificacion = calificacion;
  var self = this,
    i;
  this.bloque = "<div class='row'><div class='col-lg-12'>";

  this.iniciar = function () {
    var restantes = 5 - calificacion;
    for (i = 0; i < restantes; i++) {
      this.bloque = this.bloque.concat("<span class='estrella-negativa-grande'></span>");
    }

    for (i = 0; i < this.calificacion; i++) {
      this.bloque = this.bloque.concat("<span class='estrella-positiva-grande'></span>");
    }

    this.bloque = this.bloque.concat('</div></div>');
  };

  this.setCalificacion = function (calificacion) {
    this.bloque = "<div class='row'><div class='col-lg-12'>";
    var restantes = 5 - calificacion;

    for (i = 0; i < restantes; i++) {
      this.bloque = this.bloque.concat("<span class='estrella-negativa-grande'></span>");
    }

    for (i = 0; i < calificacion; i++) {
      this.bloque = this.bloque.concat("<span class='estrella-positiva-grande'></span>");
    }

    this.bloque = this.bloque.concat('</div></div>');
  };
};
