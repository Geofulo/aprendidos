/*global $*/
var GradoSelect = function (grado) {
  "use strict";
  var self = this;
  this.grado = grado;
  this.valorActual = grado;
  this.listaGrados = ['Ninguno', 'Universidad', 'Recibido'];
  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<label>Grado</label>" +
    "<input type='hidden' id='gradoSelectField'>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div id='mostrarGradoSelect' class='derecha'>" + this.listaGrados[grado] + "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row area-opciones'>" +
    "<div class='col-lg-1'>" +
    "<div class='reduce-slider' id='reduce-grado'></div>" +
    "</div>" +
    "<div class='col-lg-9' >" +
    "<div class='slider-general' id='grado-slider'></div>" +
    "</div>" +
    "<div class='col-lg-1'>" +
    "<div class='aumenta-slider' id='aumenta-grado'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
  this.iniciar = function () {
    $("#grado-slider").slider({
      min: 0,
      max: this.listaGrados.length - 1,
      step: 1,
      value: this.valorActual,
      animate: "fast",
      change: function (event, ui) {
        $("#gradoSelectField").val(ui.value);
        //$("#mostrarGradoSelect").text(this.listaGrados[ui.value]);
        switch (ui.value) {
          case 0:
            $("#mostrarGradoSelect").text("Ninguno");
            break;
          case 1:
            $("#mostrarGradoSelect").text("Universidad");
            break;
          case 2:
            $("#mostrarGradoSelect").text("Recibido");
        }
      }
    });
  }

  this.activarEventos = function () {
    $("#reduce-grado").click(function () {
      var grado_slider = $("#grado-slider");
      this.valorActual = grado_slider.slider('value');
      if (isNaN(this.valorActual))
        this.valorActual = 0;
      if (this.valorActual > 0)
        grado_slider.slider("value", this.valorActual - 1);
    });

    $("#aumenta-grado").click(function () {
      var grado_slider = $("#grado-slider");
      this.valorActual = grado_slider.slider("value");
      if (isNaN(this.valorActual))
        this.valorActual = 0;
      if (this.valorActual < 4) {
        grado_slider.slider("value", this.valorActual + 1);
      }
    });
  }

  this.setGrado = function (grado) {
    var grado_slider = $("#grado-slider");
    grado_slider.slider("value", grado);
  }

  this.getSlider = function () {
    return $("#grado-slider");
  }

  this.activarFiltrado = function () {
    var grado_slider = $("#grado-slider");
    grado_slider.on("slidechange", function (event, ui) {
      if (ui.value == 0) {
        delete filtro.Grado;
      } else {
        filtro.Grado = ui.value;
      }

      if (!limpiar)
        filtrar(1);
    });
  }
}
