var UltimosAlumnosItem = function (id_alumno, foto, nombre, twitter, facebook, correo, telefono, acercaDe, nota) {

  this.twitter = (twitter) ?
    "<a href='https://twiiter.com/" + twitter + "' target='_blank'><span class='twitter'></span></a>" :
    "<span class='twitter-desactivado'></span>";

  this.facebook = (facebook) ?
    "<a href='http://facebook.com/" + facebook + "' target='_blank'><span class='facebook'></span></a>" :
    "<span class='facebook-desactivado'></span>";
  this.foto = (foto) ?
    "<div class='foto' style='background-image: url(" + foto + "); background-size: cover; background-position: center'></div>" :
    "<div class='foto'></div>";

  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row ultimos-alumnos'>" +
    "<div class='col-lg-2'>" +
    this.foto +
    "</div>" +
    "<div class='col-lg-9'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='nombre'>" + nombre + "</div>" +
    this.twitter +
    this.facebook +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<span class='correo'>" + (correo ? telefono : '') + "</span>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<span class='telefono'>" + (telefono ? telefono : '') + "</span>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='acercaDe'>" + (acercaDe ? acercaDe : '') + "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='nota'>" + (nota ? nota : '') + "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
}
