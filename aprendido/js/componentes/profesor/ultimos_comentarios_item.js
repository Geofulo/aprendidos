var UltimosComentariosItem = function (foto, nombre, calificacion, comentario, nota) {
  this.nombre = nombre;
  this.calificacion = calificacion;
  this.nota = nota;
  this.foto = (foto) ?
    "<div class='foto' style='background-image: url(" + foto + "); background-size: cover; background-position: center'></div>" :
    "<div class='foto'></div>";

  this.codigoCalificacion = function () {
    var negativas = 5 - this.calificacion;
    var codigo = "";
    for (i = 0; i < negativas; i++) {
      codigo = codigo.concat('<span class="estrella-negativa-chica"></span>');
    }

    for (i = 0; i < calificacion; i++) {
      codigo = codigo.concat('<span class="estrella-positiva-chica"></span>');
    }
    return codigo;
  };

  this.bloque =
    "<div class='row ultimos-comentarios'>" +
    "<div class='col-lg-2'>" +
    this.foto +
    "</div>" +
    "<div class='col-lg-10'>" +
    "<div class='row'>" +
    "<div class='col-lg-8'>" +
    "<div class='nombre'>" +
    this.nombre +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div class='calificacion'>" +
    this.codigoCalificacion() +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='comentario'>" +
    comentario +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='nota'>" +
    this.nota;
  "</div>" +
  "</div>" +
  "</div>" +
  "</div>" +
  "</div>";
}
