var GradoNuevaMateriaSelect = function (grado) {
  var self = this;
  this.grado = grado;
  this.valorActual = grado;
  this.listaGrados = ['', 'Universidad', 'Maestría', 'Doctorado'];
  this.bloque = "<div class='row'>" +
                  "<div class='col-lg-12'>" +
                    "<div class='row'>" +
                      "<div class='col-lg-6'>" +
                        "<label>Grado</label>" +
                        "<input type='hidden' id='gradoNuevaMateriaSelectField'>" +
                      "</div>" +
                      "<div class='col-lg-6'>" +
                        "<div id='mostrarGradoNuevaMateriaSelect'>" + this.listaGrados[grado] + "</div>" +
                      "</div>" +
                    "</div>" +
                    "<div class='row area-opciones'>" +
                      "<div class='col-lg-1'>" +
                        "<div class='reduce-slider' id='reduce-grado-nueva-materia'></div>" +
                      "</div>" +
                      "<div class='col-lg-9' >" +
                        "<div class='slider-general' id='grado-slider-nueva-materia'></div>" +
                      "</div>" +
                      "<div class='col-lg-1'>" +
                        "<div class='aumenta-slider' id='aumenta-grado-nueva-materia'></div>" +
                      "</div>" +
                    "</div>" +
                  "</div>" +
                "</div>";
  this.iniciar = function() {
    $("#grado-slider-nueva-materia").slider({
      min: 0,
      max: 3,
      step: 1,
      value: this.valorActual,
      animate: "fast",
      change: function (event, ui) {
        $("#gradoNuevaMateriaSelectField").val(ui.value);
        switch (ui.value) {
        case 0:
          $("#mostrarGradoNuevaMateriaSelect").text("Ninguno");
            break;
        case 1:
          $("#mostrarGradoNuevaMateriaSelect").text("Universidad");
          break;
        case 2:
          $("#mostrarGradoNuevaMateriaSelect").text("Maestría");
          break;
        case 3:
          $("#mostrarGradoNuevaMateriaSelect").text("Doctorado");
        }
      }
    });
  }

  this.activarEventos = function () {
    $("#reduce-grado-nueva-materia").click(function() {
      var grado_slider_nueva_materia = $("#grado-slider-nueva-materia");
      this.valorActual = grado_slider_nueva_materia.slider('value');
      if (isNaN(this.valorActual))
        this.valorActual = 0;
      if (this.valorActual > 0)
        grado_slider_nueva_materia.slider("value", this.valorActual - 1);
    });

    $("#aumenta-grado-nueva-materia").click(function() {
      grado_slider_nueva_materia = $("#grado-slider-nueva-materia");
      this.valorActual = grado_slider_nueva_materia.slider("value");
      if (isNaN(this.valorActual))
        this.valorActual = 0;
      if (this.valorActual < 4) {
        grado_slider_nueva_materia.slider("value", this.valorActual + 1);
      }
    });
  }

  this.setGrado = function(grado) {
    grado_slider_nueva_materia = $("#grado-slider-nueva-materia");
    grado_slider_nueva_materia.slider("value", grado);
  }
}