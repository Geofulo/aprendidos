$(document).ready(function() {
              
                $("#guardarCambiosPerfilProfesor").click(function(event) {
                  event.preventDefault();
                  el = new Everlive({
        appId: 'meo70jlpoo8yurxj',
        authentication: {
          persist: true
        }
      });
                  var datosProfesor = el.data('Profesor');
                  
                  var valoresProfesorAGuardar = {
                      Id: data.result.Id,
                      Telefono: $("#telefonoProfesorPerfil").val(),
                      TwitterUrl: $("#twitterProfesorPerfil").val(),
                      FacebookUrl: $("#facebookProfesorPerfil").val(),
                      LinkedinUrl: $("#linkedinProfesorPerfil").val(),
                      Descripcion: $("#sobreMiProfesorPerfil").val(),
                      Direccion: $("#direccionProfesorPerfil").val(),
                      Precio: $("#precioMaximoField").val(),
                      ClaseGrupo: ($("#claseGrupalField").val() == '1')?true:false,
                      ClasePersonal: ($("#claseIndividualField").val() == '1')?true:false,
                      VaADomicilio: ($("#vaADomicilioField").val() == '1')?true:false,
                      Grado: $("#gradoSelectField").val(),
                      HorarioDia: ($("#horarioDiaField").val() == '1')?true:false,
                      HorarioTarde: ($("#horarioTardeField").val() == '1')?true:false,
                      HorarioNoche: ($("#horarioNocheField").val() == '1')?true:false
                  };
                  
                  datosProfesor.updateSingle(
                    valoresProfesorAGuardar,
                    function(data) {
                      console.log("Se guardo el profesor");
                      console.log(data);
                    }, function(error) {
                      console.log("No se guardo el profesor ");
                      console.log(error);
                    }
                  );
                  
                  
                  var valoresUsuarioAGuardar = {
                    Id: data.result.Id,
                    DisplayName: $("#nombreProfesorPerfil").val(),
                    Email: $("#emailProfesorPerfil").val()
                  }
                
                  
                  el.Users.updateSingle(
                    valoresUsuarioAGuardar, 
                    function(data) {
                      console.log("Se guardo el usuario");
                      console.log(data);
                  }, function(error) {
                      console.log("No se guardo el usuario");
                      console.log(error);
                  });
                  
                  window.location.reload();
                });
              
                var sliderPrecio = new SlidePrecio('Un precio', 0, 100, 5, 4);
                $("#precioMaximoSlide").html(sliderPrecio.bloque);
                sliderPrecio.iniciar();
                sliderPrecio.activarEventos();

                var tipoGrupoSelect = new TipoGrupoSelect('Clases');
                $("#tipoGrupoSelect").html(tipoGrupoSelect.bloque);
                tipoGrupoSelect.activarEventos();

                var vaADomicilioSelect = new VaADomicilioSelect(false);
                $("#vaADomicilioSelect").html(vaADomicilioSelect.bloque);
                vaADomicilioSelect.activarEventos();

                var gradoSelect = new GradoSelect(0);
                $("#gradoSelect").html(gradoSelect.bloque);
                gradoSelect.iniciar();
                gradoSelect.activarEventos();

                var horarioSelect = new HorarioSelect();
                $("#horariosSelect").html(horarioSelect.bloque);
                horarioSelect.activarEventos();

                horarioSelect.activarHorarioNoche();
                horarioSelect.activarHorarioTarde();

                unAlumno = new UltimosAlumnosItem('12', false, 'alumno1', false,'facebook1', 'correo1', '5555555', 'biografia1', 'nota1');
                dosAlumno = new UltimosAlumnosItem('13', false, 'alumno2', 'twitter2','facebook2', 'correo2', '8888888','biografia2', 'nota2');
                tresAlumno = new UltimosAlumnosItem('14', false, 'alumno3', 'twitter3',false, 'correo3', '1112211','biografia3', 'nota3');

                $("#listaUltimosAlumnos").append(unAlumno.bloque);
                $("#listaUltimosAlumnos").append(dosAlumno.bloque);
                $("#listaUltimosAlumnos").append(tresAlumno.bloque);

                var unaMateria = new MateriaItem('materia1', 'universidad');
                var otraMateria = new MateriaItem('materia2', 'prepa');

                $("#listaMateriasProfesor").append(unaMateria.bloque).append(otraMateria.bloque);


                var usuario = data.result;
                var profesor = el.data('Profesor');
              
                profesor.getById(usuario.Id).then(
                    function(data) {
                        var profesor = data.result;
                        $("#nombreProfesorPerfil").val(usuario.DisplayName);
                        $("#emailProfesorPerfil").val(usuario.Email);
                        $("#telefonoProfesorPerfil").val(profesor.Telefono);
                        $("#twitterProfesorPerfil").val(profesor.TwitterUrl);
                        $("#facebookProfesorPerfil").val(profesor.FacebookUrl);
                        $("#linkedinProfesorPerfil").val(profesor.LinkedinUrl);
                        $("#sobreMiProfesorPerfil").val(profesor.Descripcion);
                        $("#descripcionBiografiaProfesor").text(profesor.Descripcion);

                        //$("#ciudadProfesorPerfil").val()
                        $("#direccionProfesorPerfil").val(profesor.Direccion);


                        sliderPrecio.setPrecio(profesor.Precio);

                        if (profesor.ClasesGrupo) {
                            tipoGrupoSelect.activarClasesGrupales();
                        } else {
                            tipoGrupoSelect.desactivarClasesGrupales();
                        }

                        if (profesor.ClasesPersonal) {
                            tipoGrupoSelect.activarClasesIndividuales();
                        } else {
                            tipoGrupoSelect.desactivarClasesIndividuales();
                        }

                        if (profesor.VaADomicilio) {
                            vaADomicilioSelect.activarVaADomicilio();
                        } else {
                            vaADomicilioSelect.desactivarVaADomicilio();
                        }

                        gradoSelect.setGrado(profesor.Grado);
                      
                        if (profesor.HorarioDia) {
                          horarioSelect.activarHorarioDia();
                          $("#horarioDiaProfesorPerfil").addClass('horario-dia-activado-profesor-resultado');
                        } else {
                          horarioSelect.desactivarHorarioDia();
                          $("#horarioDiaProfesorPerfil").addClass('horario-dia-desactivado-profesor-resultado');
                        }

                        if (profesor.HorarioTarde) {
                          horarioSelect.activarHorarioTarde();
                          $("#horarioTardeProfesorPerfil").addClass('horario-tarde-activado-profesor-resultado');
                        } else {
                          horarioSelect.desactivarHorarioTarde();
                          $("#horarioTardeProfesorPerfil").addClass('horario-tarde-desactivado-profesor-resultado');
                        }

                        if (profesor.HorarioNoche) {
                          horarioSelect.activarHorarioNoche();
                          $("#horarioNocheProfesorPerfil").addClass('horario-noche-activado-profesor-resultado');
                        } else {
                          horarioSelect.desactivarHorarioNoche();
                          $("#horarioNocheProfesorPerfil").addClass('horario-noche-desactivado-profesor-resultado');
                        }
                    }, function(error) {
                        window.location.replace("/aprendido");
                    }
                );
            });