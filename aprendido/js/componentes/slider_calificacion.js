var SliderCalificacion = function (titulo, calificacion) {
  this.titulo = titulo;
  this.calificacion = calificacion;

  this.bloque = "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<label>" + titulo + "</label>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div id='mostrarCalificacionActualSlider' class='derecha'>" + this.calificacion + "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-12'>" +
    "<input type='hidden' id='calificacionField'>" +
    "<div class='row area-opciones'>" +
    "<div class='col-lg-1'>" +
    "<div class='reduce-slider' id='reduce-calificacion'></div>" +
    "</div>" +
    "<div class='col-lg-9' >" +
    "<div>" +
    "<div class='slider-general' id='calificacion-slider'></div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-1'>" +
    "<div class='aumenta-slider' id='aumenta-calificacion'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.iniciar = function () {
    $("#calificacion-slider").slider({
      min: 0,
      max: 5,
      step: 1,
      animate: "fast",
      value: this.calificacion,
      change: function (event, ui) {
        $("#mostrarCalificacionActualSlider").text(ui.value);
      }
    });
  };

  this.activarEventos = function () {
    $("#reduce-calificacion").click(function () {
      calificacion_slider = $("#calificacion-slider");
      mostrarCalificacionActualSlider = $("#mostrarCalificacionActualSlider");
      valorActual = calificacion_slider.slider("value");
      if (valorActual > 0) {
        calificacion_slider.slider("value", (valorActual - 1));
        mostrarCalificacionActualSlider.text(calificacion_slider.slider("value"));
      }
    });
    $("#aumenta-calificacion").click(function () {
      calificacion_slider = $("#calificacion-slider");
      valorActual = calificacion_slider.slider("value");
      mostrarCalificacionActualSlider = $("#mostrarCalificacionActualSlider");
      if (valorActual < 5) {
        calificacion_slider.slider("value", (valorActual + 1));
        mostrarCalificacionActualSlider.text(calificacion_slider.slider("value"));
      }
    });
  }

  this.getSlider = function () {
    return $("#calificacion-slider");
  };

  this.setCalificacion = function (calificacion) {
    $("#calificacion-slider").slider("value", calificacion);
  }

  this.activarFiltrado = function () {
    $("#calificacion-slider").on("slidechange", function (event, ui) {
      if (ui.value == 0) {
        delete filtro.Puntaje;
      } else {
        filtro.Puntaje = ui.value;
      }
      if (!limpiar)
        filtrar(1);
    })
  }
};
