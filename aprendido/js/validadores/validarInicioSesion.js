$(document).ready(function () {
  $("#formularioInicioSesion").validate({
    rules: {
      login_username: "required",
      login_password: "required"
    },
    messages: {
      login_username: "Indica tu nombre de usuario",
      login_password: "Indica tu contraseña"
    },
    errorPlacement: function (error, element) {
      error.insertBefore(element);
    }
  });
});
