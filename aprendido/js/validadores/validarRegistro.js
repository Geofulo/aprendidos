/*globals $*/

$(document).ready(function () {
  "use strict";
  $("#formularioRegistro").validate({
    rules: {
      tipoUsuario: "required",
      username: "required",
      register_password: "required",
      register_confirmPassword: {
        required: true,
        equalTo: "#register_password"
      }
    },
    messages: {
      tipoUsuario: "Debes elegir una opción.",
      username: "Debes indicar un nombre de usuario",
      register_password: "Por favor proporciona una contraseña.",
      register_confirmPassword: "Debes escribir la misma contraseña."
    },
    errorPlacement: function (error, element) {
      if (element.attr('name') === 'tipoUsuario') {
        error.insertAfter("#error-tipoUsuario");
      } else {
        error.insertBefore(element);
      }
    }
  });
});
