/*globals console*/
var Cuerpo = function (parametros) {
  "use strict";
  var datosUsuario = parametros.profesorUsuario.result[0],
    datosProfesor = parametros.profesor;
  console.log(parametros);
  this.html =
    "<div class='contenedor-interno perfil'>" +
    "<form id='formuilarioEditarProfesor'>" +
    "<div class='row' id='perfil-profesor-publico'>" +
    "<div class='col-lg-3'>" +
    "<div id='foto_perfil' class='foto'>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<div class='nombre' id='nombre'>" +
    datosUsuario.DisplayName +
    "</div>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div class='calificacion' id='calificacion'>" +
    parametros.calificacion.bloque +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='acercaDe'>" +
    datosProfesor.Descripcion +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-3'>" +
    "<div id='gradoProfesorPerfil'>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span id='claseIndividual' class='icono'></span>" +
    "<span id='claseGrupal' class='icono'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span id='horarioDiaProfesor' class='icono'></span>" +
    "<span id='horarioTardeProfesor' class='icono'></span>" +
    "<span id='horarioNocheProfesor' class='icono'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span id='vaADomicilioProfesor' class='icono'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row separacion-vertical-izquierda'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span class='icono-precio'></span>" +
    "<span class='precio-profesor-resultado' id='cantidadPrecio'></span>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<input type='button' class='chico' value='pedir clase' id='pedirClase'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-6 col-lg-offset-3 contacto'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 subtitulo'>" +
    "Contacto" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<div id='emailProfesor'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='telefonoProfesor'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<div id='twitterProfesor'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='facebookProfesor'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='linkedinProfesor'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<section>" +
    "<div class='titulo'>" +
    "Sobre mi" +
    "</div>" +
    "</section>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div id='biografiaProfesor'>" +
    datosProfesor.Descripcion +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<section>" +
    "<div class='titulo'>" +
    "Características" +
    "</div>" +
    "</section>" +
    "</div>" +
    "</div>" +
    "<div cla+ss='row'>" +
    "<section class='col-lg-12 sobresaliente-cb'>" +
    "<div class='row'>" +
    "<div class='col-lg-2'>" +
    "<div class='subtitulo ciudad'>Ciudad</div>" +
    "<div id='ciudadProfesor'></div>" +
    "</div>" +
    "<div class='col-lg-10'>" +
    "<div class='subtitulo'>Dirección</div>" +
    "<div id='direccionProfesor'></div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='descripcionOpcionProfesor'>" +
    "<div class='texto precio'>Precio por hora</div>" +
    "<div id='precioProfesor' class='valorOpcionProfesor'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='descripcionOpcionProfesor'>" +
    "<div class='texto grado'>Grado</div>" +
    "<div id='gradoProfesor' class='valorOpcionProfesor'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4 separacion-vertical-izquierda'>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='clasesIndividualesProfesor'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='clasesGrupalesProfesor'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='vaADomicilioProfesor2'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4 separacion-vertical-izquierda'>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='horarioDiaProfesor2'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='horarioTardeProfesor2'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='horarioNocheProfesor2'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "</div>" +
    "<div class='row'>" +
    "<section class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo'>" +
    "Materias" +
    "</div>" +
    "</div>" +
    "<div class='row' id='listaMateriasProfesor'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<section class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo'>" +
    "Últimos comentarios" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='listaUltimosComentarios'>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-derecha' id='verMasComentarios'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>";
};
