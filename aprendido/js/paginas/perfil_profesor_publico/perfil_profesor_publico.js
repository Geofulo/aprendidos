/*global Everlive,$,everlive,alert,console,$,MateriaItem,UltimosComentariosItem,Menu,Navegacion,CabeceraGeneral,Cuerpo,Calificacion*/

everlive.Users.currentUser().then(
  function (usuario) {
    "use strict";
    var profesor = null,
      parametros = null,
      profesorQuery = null,
      nombreGrados = ['Ninguno', 'Universidad', 'Maestría', 'Doctorado'],
      listaMateriasProfesor = [],
      listaUltimosComentarios = [],
      cuerpo = null,
      calificacion = null,
      alumno = null;

    if (window.location.search === '') {
      window.location.replace('/aprendido/');
    } else {
      parametros = window.location.search.substring(1).split("=");
      if (parametros.length === 2) {
        profesor = parametros[1];
      } else if (window.location.search !== '') {
        profesor = window.location.search.substring(1);
      } else {
        window.location.replace("/aprendido/");
      }
    }

    everlive.Users.get({
      Username: profesor
    }, function (profesorUsuario) {
      everlive.data('Profesor').getById(profesorUsuario.result[0].Id)
        .then(
          function (profesor) {
            profesor = profesor.result;
            var menu = null,
              menuSesion = null,
              navegacion = new Navegacion(),
              cabecera = null,
              modalSeleccionarMateria = new ModalSeleccionarMateria(),
              modalInicioSesion = new ModalInicioSesion(),
              modalRegistro = new ModalRegistro(),
              pie = new Pie();

            if (usuario.result === null) {
              menu = new Menu('quieroAprender', false);
            } else {
              menuSesion = new MenuSesion(usuario.result);
              menu = new Menu('quieroAprender', true, menuSesion);
            }
            navegacion.setMenu(menu);


            calificacion = new Calificacion(profesor.Puntaje);
            calificacion.iniciar();

            cuerpo = new Cuerpo({
              profesor: profesor,
              profesorUsuario: profesorUsuario,
              calificacion: calificacion
            });


            cabecera = new CabeceraGeneral({
              navegacion: navegacion,
              titulo: 'Quiero enseñar',
              subtitulo: 'Perfil profesor'
            });

            $("body").append(cabecera.html);
            $("body").append(cuerpo.html);
            $("body").append(pie.html);


            if (menuSesion !== null) menuSesion.activarEventos();
            menu.activarEventos();

            $("#dialogos").append(modalSeleccionarMateria.html);
            $("#dialogos").append(modalInicioSesion.html);
            $("#dialogos").append(modalRegistro.html);
            $("#cantidadPrecio").text(profesor.Precio);
            $("#precioProfesor").text("$ " + profesor.Precio);
            $("#gradoProfesor").text(nombreGrados[profesor.Grado]);

            modalSeleccionarMateria.activarEventos();
            modalInicioSesion.activarEventos();
            modalRegistro.activarEventos();

            if (!profesor.hasOwnProperty('ClasesPersonal') || !profesor.ClasesPersonal) {
              $("#clasesIndividualesProfesor").html("<div class='clasesIndividuales2Desactivadas'>");
              $("#claseIndividual").addClass('individualDesactivado');
            } else {
              $("#clasesIndividualesProfesor").html("<div class='descripcionOpcionProfesor'>" +
                "<div class='texto clasesIndividuales'>Clases</div>" +
                "<div id='clasesIndividualesProfesor' class='valorOpcionProfesor'>Individuales</div>" +
                "</div>");
              $("#claseIndividual").addClass('individualActivado');
            }

            if (!profesor.hasOwnProperty('ClasesGrupo') || !profesor.ClasesGrupo) {
              $("#clasesGrupalesProfesor").html("<div class='clasesGrupales2Desactivadas'>");
              $("#claseGrupal").addClass('grupalDesactivado');
            } else {
              $("#clasesGrupalesProfesor").html("<div class='descripcionOpcionProfesor'>" +
                "<div class='texto clasesGrupales'>Clases</div>" +
                "<div id='clasesGrupalesProfesor' class='valorOpcionProfesor'>Grupales</div>" +
                "</div>");
              $("#claseGrupal").addClass('grupalActivado');
            }

            if (!profesor.hasOwnProperty('HorarioDia') || !profesor.HorarioDia) {
              $("#horarioDiaProfesor2").html("<div class='horarioDia2Desactivadas'>");
              $("#horarioDiaProfesor").addClass('horarioDiaDesactivado');
            } else {
              $("#horarioDiaProfesor2").html("<div class='descripcionOpcionProfesor'>" +
                "<div class='texto horarioDia'>Horarios</div>" +
                "<div class='valorOpcionProfesor'>Día</div>" +
                "</div>");
              $("#horarioDiaProfesor").addClass('horarioDiaActivado');
            }

            if (!profesor.hasOwnProperty('HorarioTarde') || !profesor.HorarioTarde) {
              $("#horarioTardeProfesor").addClass('horarioTardeDesactivado');
              $("#horarioTardeProfesor2").html("<div class='horarioTarde2Desactivadas'>");
            } else {
              $("#horarioTardeProfesor").addClass('horarioTardeActivado');
              $("#horarioTardeProfesor2").html("<div class='descripcionOpcionProfesor'>" +
                "<div class='texto horarioTarde'>Horarios</div>" +
                "<div class='valorOpcionProfesor'>Tarde</div>" +
                "</div>");
            }

            if (!profesor.hasOwnProperty('HorarioNoche') || !profesor.HorarioNoche) {
              $("#horarioNocheProfesor2").html("<div class='horarioNoche2Desactivadas'>");
              $("#horarioNocheProfesor").addClass('horarioNocheDesactivado');
            } else {
              $("#horarioNocheProfesor").addClass('horarioNocheActivado');
              $("#horarioNocheProfesor2").html("<div class='descripcionOpcionProfesor'>" +
                "<div class='texto horarioNoche'>Horarios</div>" +
                "<div class='valorOpcionProfesor'>Noche</div>" +
                "</div>");
            }

            if (!profesor.hasOwnProperty('VaADomicilio') || !profesor.VaADomicilio) {
              $("#vaADomicilioProfesor").addClass('vaADomicilioDesactivado');
              $("#vaADomicilioProfesor2").html("<div class='vaADomicilio2Desactivadas'>");
            } else {
              $("#vaADomicilioProfesor").addClass('vaADomicilioActivado');
              $("#vaADomicilioProfesor2").html("<div class='descripcionOpcionProfesor'>" +
                "<div class='texto vaADomicilio'>¿Va a domicilio?</div>" +
                "<div id='vaADomicilioProfesor' class='valorOpcionProfesor'>Sí</div>" +
                "</div>");
            }

            if (profesor.hasOwnProperty('Materias') && profesor.Materias.length > 0) {
              $.each(profesor.Materias, function (index, item) {
                everlive.data('Materia').getById(item).then(
                  function (data) {
                    var materiaItem = new MateriaItem(data.result.Nombre, nombreGrados[profesor.Grado], data.result.Id, false);
                    $("#listaMateriasProfesor").append(materiaItem.bloque);
                  }
                );
              });
              $("#pedirClase").click(function (event) {
                if (usuario.result === null) {
                  $("#modalInicioSesion").show();
                } else {
                  $("#formularioSeleccionMateria").append("<div class='row'><div class='col-lg-12 contenido-centrado'><input type='submit' value='Confirmar' id='confirmarTomarMateria' class='boton-chico'></div></div>");
                  $("#confirmarTomarMateria").click(function (event) {
                    event.preventDefault();
                    everlive.Users.currentUser().then(
                      function (data) {
                        console.log("Se registrara la materia");
                        console.log($("input[name='materiaATomar']:checked").val());
                        console.log("Del profesor");
                        console.log(profesor.Id);
                        console.log("Y el alumno");
                        console.log(data.result.Id);
                        everlive.data('Clase').create({
                          Materia: $("input[name='materiaATomar']:checked").val(),
                          Alumno: data.result.Id
                        }, function (data) {
                          console.log("LA clase creada es:");
                          console.log(data);
                          var claseCreada = data.result.Id;
                          everlive.data('Profesor').getById(profesor.Id).then(
                            function (data) {
                              console.log("El profesor obtenido es");
                              console.log(data);
                              if (data.result.hasOwnProperty('Clases') && Array.isArray(data.result.Clases)) {
                                var clasesProfesor = data.result.Clases;
                              } else {
                                var clasesProfesor = [];
                              }
                              console.log(data);
                              clasesProfesor.push(claseCreada);
                              console.log("Las clases del profesor son");
                              console.log(clasesProfesor);
                              everlive.data('Profesor').updateSingle({
                                  Id: profesor.Id,
                                  Clases: clasesProfesor
                                },
                                function (data) {
                                  console.log("Al profesor se le asigno la clase");
                                  console.log(data);
                                  $("#modalSeleccionMateria").hide();
                                },
                                function (error) {
                                  console.log("Error al actualizar al profesor");
                                  console.log(error);
                                });
                            }
                          );
                          $("#emailProfesor").text(usuario.Email);
                          $("#telefonoProfesor").text(profesor.Telefono);

                          if (!profesor.hasOwnProperty('TwitterUrl') || !profesor.TwitterUrl) {
                            $("#twitterProfesor").addClass('twitterDesactivado');
                          } else {
                            $("#twitterProfesor").addClass('twitterActivado');
                          }

                          if (!profesor.hasOwnProperty('FacebookUrl') || !profesor.FacebookUrl) {
                            $("#facebookProfesor").addClass('facebookDesactivado');
                          } else {
                            $("#facebookProfesor").addClass('facebookActivado');
                          }

                          if (!profesor.hasOwnProperty('LinkedinUrl') || !profesor.LinkedinUrl) {
                            $("#linkedinProfesor").addClass('linkedinDesactivado');
                          } else {
                            $("#linkedinProfesor").addClass('linkedinActivado');
                          }
                        }, function (error) {

                        })
                      },
                      function (error) {

                      }
                    );
                  });
                  $("#modalSeleccionMateria").show();
                }
              });
            } else {
              $("#pedirClase").hide();
              $("#listaMateriasProfesor").html("<em>El profesor aún no tiene materias.</em>");
            }

            var materiasProfesor = profesor.Materias;
            everlive.Users.currentUser().then(
              function (data) {
                alumno = data.result;
                if (profesor.hasOwnProperty('Clases') && Array.isArray(profesor.Clases)) {
                  var query = new Everlive.Query();
                  query.where().isin('Id', profesor.Clases).eq('Alumno', alumno.Id);
                  everlive.data('Clase').get(query,
                    function (data) {
                      if (data.result.length >= 1) {
                        $("#verMasComentarios").html("<a href='#' class='mas-resultados'>ver más</a>");

                        $("#emailProfesor").text(usuario.Email);
                        $("#telefonoProfesor").text(profesor.Telefono);

                        if (!profesor.hasOwnProperty('TwitterUrl') || !profesor.TwitterUrl) {
                          $("#twitterProfesor").addClass('twitterDesactivado');
                        } else {
                          $("#twitterProfesor").addClass('twitterActivado');
                        }

                        if (!profesor.hasOwnProperty('FacebookUrl') || !profesor.FacebookUrl) {
                          $("#facebookProfesor").addClass('facebookDesactivado');
                        } else {
                          $("#facebookProfesor").addClass('facebookActivado');
                        }

                        if (!profesor.hasOwnProperty('LinkedinUrl') || !profesor.LinkedinUrl) {
                          $("#linkedinProfesor").addClass('linkedinDesactivado');
                        } else {
                          $("#linkedinProfesor").addClass('linkedinActivado');
                        }
                      }
                      $.each(data.result, function (index, item) {
                        materiasProfesor.splice(materiasProfesor.indexOf(item.Materia), 1);
                      });
                      $.each(materiasProfesor, function (index, item) {
                        everlive.data('Materia').getById(item).then(
                          function (data) {
                            $("#formularioSeleccionMateria").append("<input type='radio' name='materiaATomar' value='" + data.result.Id + "'> " + data.result.Nombre + "<br>");
                          }
                        );
                      });
                    });
                } else {
                  $("#listaUltimosComentarios").html("<em>El profesor aún no imparte clases</em>");
                  $.each(profesor.Materias, function (index, item) {
                    everlive.data('Materia').getById(item).then(
                      function (data) {
                        $("#formularioSeleccionMateria").append("<input type='radio' name='materiaATomar' value='" + data.result.Id + "'> " + data.result.Nombre + "<br>");
                      }
                    );
                  });
                }
              }
            );
          }
        );
    });
  }
);
