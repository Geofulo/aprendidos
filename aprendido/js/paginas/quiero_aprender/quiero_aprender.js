/*global $,everlive, Menu, Navegacion, Pie, ModalInicioSesion, ModalRegistro, ModalTipoUsuario, CabeceraGeneral, CuerpoIndex, Cabecera,GradoSelect,VaADomicilioSelect,SliderCalificacion,TipoGrupoSelect,SlidePrecio,HorarioSelect,console,Resultados,iniciarFiltro,filtro:true,inicial,limpiar:true,filtrar*/
everlive.Users.currentUser().then(
  function (usuario) {
    "use strict";
    everlive.data('Ciudad').get().then(
      function (ciudades) {
        everlive.data('Materia').get().then(
          function (materias) {
            var menu = null,
              navegacion = new Navegacion(),
              pie = new Pie(),
              modalInicioSesion = new ModalInicioSesion(),
              modalRegistro = new ModalRegistro(),
              gradoSelect = new GradoSelect(0),
              vaADomicilioSelect = new VaADomicilioSelect(false),
              sliderCalificacion = new SliderCalificacion("Puntaje mínimo", 0),
              tipoGrupoSelect = new TipoGrupoSelect('Clases'),
              sliderPrecio = new SlidePrecio("Precio Máximo", 0, 500, 50, 0),
              horarioSelect = new HorarioSelect(),
              parametrosCabecera = {
                materias: materias.result,
                ciudades: ciudades.result,
                materiaBuscada: null,
                ciudadBuscada: null,
                grado: gradoSelect,
                vaADomicilio: vaADomicilioSelect,
                puntaje: sliderCalificacion,
                clases: tipoGrupoSelect,
                precio: sliderPrecio,
                horario: horarioSelect
              },
              resultados = new Resultados(),
              cabecera = null,
              menuSesion = null;

            if (usuario.result !== null) {
              menuSesion = new MenuSesion(usuario.result);
              menu = new Menu('quieroAprender', true, menuSesion);
              navegacion.setMenu(menu);

            } else {
              menu = new Menu('quieroAprender', false);
              navegacion.setMenu(menu);
            }
            parametrosCabecera.navegacion = navegacion;
            cabecera = new Cabecera(parametrosCabecera);
            $("footer").append(pie.html);

            $("body")
              .prepend(cabecera.html);
            $("body")
              .append(pie.html);
            $("main")
              .append(resultados.html);
            $("#dialogos")
              .append(modalInicioSesion.html)
              .append(modalRegistro.html)

            gradoSelect.iniciar();
            gradoSelect.activarEventos();
            vaADomicilioSelect.activarEventos();
            tipoGrupoSelect.activarEventos();
            menu.activarEventos();
            sliderCalificacion.iniciar();
            sliderCalificacion.activarEventos();
            sliderPrecio.iniciar();
            sliderPrecio.activarEventos();
            horarioSelect.activarEventos();
            modalInicioSesion.activarEventos();
            modalRegistro.activarEventos();
            iniciarFiltro();
            gradoSelect.activarFiltrado();
            sliderCalificacion.activarFiltrado();
            sliderPrecio.activarFiltrado();
            tipoGrupoSelect.activarFiltrado();
            horarioSelect.activarFiltrado();
            vaADomicilioSelect.activarFiltrado();

            /**
             * 
             */
            $("#quieroAprenderCiudadSelect").on('change', function (event) {
              var ciudad = $("#quieroAprenderCiudadSelect").val();
              console.log("Ciudad: " + ciudad);
              if (ciudad === "") {
                delete filtro.Ciudades;
              } else {
                filtro.Ciudades = ciudad;
              }

              if (!inicial && !limpiar) {
                filtrar(1);
              }
            });

            $("#quieroAprenderMateriaSelect").on('change', function (event) {
              var materia = $("#quieroAprenderMateriaSelect").val();
              console.log(materia);
              if (materia === '') {
                delete filtro.Materias;
              } else {
                filtro.Materias = materia;
              }

              if (!inicial && !limpiar) {
                filtrar(1);
              }
            });


            $("#ordenarSelect").on('change', function (event) {
              if (!limpiar) {
                filtrar(1);
              }
            });

            $("#limpiarFiltrosBtn").click(function (event) {
              limpiar = true;
              sliderCalificacion.setCalificacion(0);
              sliderPrecio.setPrecio(0);
              gradoSelect.setGrado(0);
              $("#quieroAprenderCiudadSelect").val("").trigger("change");
              $("#quieroAprenderMateriaSelect").val("").trigger("change");
              vaADomicilioSelect.desactivarVaADomicilio();
              horarioSelect.desactivarHorarioDia();
              horarioSelect.desactivarHorarioTarde();
              horarioSelect.desactivarHorarioNoche();
              tipoGrupoSelect.desactivarClasesIndividuales();
              tipoGrupoSelect.desactivarClasesGrupales();
              filtro = {};
              filtrar(1);
              limpiar = false;
            });
          },
          function (errorMaterias) {
            console.log("Error al obtener las materias");
            console.log(errorMaterias);
          }
        );
      },
      function (errorCiudades) {
        console.log("Error al obtener las ciudades");
        console.log(errorCiudades);
      }
    );
  }
);
