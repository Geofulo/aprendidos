var ResultadoProfesor = function (opciones) {
  /*
   * Opciones:
   * nombreProfesor, descripcionProfesor, calificacion, precio, clasesIndividuales, clasesGrupales, horarioDia, horarioTarde, horarioNoche, vaADomicilio, usuarioProfesor, imagen
   */
  this.calificacion = new Calificacion(opciones.calificacion);
  this.calificacion.iniciar();

  this.nombreProfesor = opciones.nombreProfesor;
  this.descripcionProfesor = opciones.descripcionProfesor;
  this.bloqueResultado = "<div class='row resultado-profesor'>" +
    "<div class='col-lg-2'>" +
    "<div class='foto-resultado-profesor' " + (opciones.hasOwnProperty('imagen') ? "style='background-image: url(" + opciones.imagen + "); background-size: cover; background-position: center'" : "'") + ">" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-8 informacion-profesor-resultado'>" +
    "<div class='row'>" +
    "<div class='col-lg-7 nombre-profesor-resultado'>" +
    opciones.nombreProfesor +
    "</div>" +
    "<div class='col-lg-5'>" +
    this.calificacion.bloque +
    "</div>" +
    "<div class='row descripcion-profesor-resultado'>" +
    "<div class='col-lg-12'>" +
    opciones.descripcionProfesor +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-3'>" +
    "<span class='grado-profesor-resultado'></span>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    ((opciones.clasesIndividuales) ? "<span class='resultadoProfesor individualActivado'></span>" : "<span class='resultadoProfesor individualDesactivado'></span>") +
    ((opciones.clasesGrupales) ? "<span class='resultadoProfesor grupalActivado'></span>" : "<span class='resultadoProfesor grupalDesactivado'></span>") +
    "</div>" +
    "<div class='col-lg-3'>" +
    // Indicador de horario -->
    ((opciones.horarioDia) ? "<span class='resultadoProfesor icono horario-dia-activado'></span>" : "<span class='resultadoProfesor icono horario-dia-desactivado'></span>") +
    ((opciones.horarioTarde) ? "<span class='resultadoProfesor icono horario-tarde-activado'></span>" : "<span class='resultadoProfesor icono horario-tarde-desactivado'></span>") +
    ((opciones.horarioNoche) ? "<span class='resultadoProfesor icono horario-noche-activado'></span>" : "<span class='resultadoProfesor icono horario-noche-desactivado'></span>") +
    "</div>" +
    "<div class='col-lg-3'>" +
    // va a domicilio -->
    ((opciones.vaADomicilio) ? "<span class='resultadoProfesor icono va-a-domicilio-activado'></span>" : "<span class='resultadoProfesor icono va-a-domicilio-desactivado'></span>") +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-2 separacion-precio-perfil-profesor-resultado contenido-centrado'>" +
    // Componente de precio -->
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span class='icono-precio-profesor-resultado'></span>" +
    "<span class='precio-profesor-resultado' id='profesor_indice_precio'>" + opciones.precio + "</span>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    // esta accion cambia en otra plantilla -->
    "<form method='get' action='perfil_profesor_publico.html'>" +
    "<input type='hidden' name='p' value='" + opciones.usuarioProfesor + "'>" +
    "<input type='submit' class='mini' value='ver perfil'/>" +
    "</form>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
}
