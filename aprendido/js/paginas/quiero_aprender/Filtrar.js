var filtro = {};

var parametros = window.location.search.substr(1).split('&');
var ciudadBuscada = null;
var materiaBuscada = null;
var filtroActual = {};

var filtro = {};

var limpiar = false;
var inicial = false;

function iniciarFiltro() {
  for (var i = 0; i < parametros.length; i++) {
    var parametro = parametros[i].split('=');
    if (parametro[0] == 'ciudadSelect') {
      $("#quieroAprenderCiudadSelect option[value='" + parametro[1] + "']").attr("selected", "selected");
      filtro.Ciudades = parametro[1];
    } else if (parametro[0] == 'materiaSelect') {
      $("#quieroAprenderMateriaSelect option[value='" + parametro[1] + "']").attr("selected", "selected");
      filtro.Materias = parametro[1];
    }
  }
}

function filtrar(pagina) {
  var ordenar = $("#ordenarSelect").val();
  var resultadosMostrados = 10;

  var query = new Everlive.Query();
  query = query.where();
  for (var propiedad in filtro) {
    query = query.equal(propiedad, filtro[propiedad]);
  };

  query = query.done();
  if (ordenar != '')
    query = query.order(ordenar);

  query = query.skip((pagina - 1) * resultadosMostrados).take(resultadosMostrados);
  filtroActual = filtro;


  everlive.data('Profesor').get(query).then(
    function (data) {
      console.log("Filtro");
      console.log(filtro);
      console.log("obtubo");
      console.log(data);
      $("#contenidoEnlacesPaginas").empty();
      $("#resultados_profesores").empty();
      $("#cantidadResultados").text(data.count);

      $.each(data.result, function (index, item) {
        var cantidadPaginas = Math.ceil(data.count / resultadosMostrados);
        $("#contenidoEnlacesPaginas").empty();
        $("#contenidoEnlacesPaginasAbajo").empty();

        for (var i = 0; i < cantidadPaginas; i++) {
          var enlacePaginacion = "<div class='opcion-paginacion numero-pagina " + ((pagina == (i + 1)) ? 'opcion-paginacion-activa' : '') + "' id='cambiarPagina_" + (i + 1) + "'>" + (i + 1) + "</div>";
          var enlacePaginacionAbajo = "<div class='opcion-paginacion numero-pagina " + ((pagina == (i + 1)) ? 'opcion-paginacion-activa' : '') + "' id='cambiarPaginaAbajo_" + (i + 1) + "'>" + (i + 1) + "</div>";
          $("#contenidoEnlacesPaginas").append(enlacePaginacion);
          $("#contenidoEnlacesPaginasAbajo").append(enlacePaginacionAbajo);

          $("#cambiarPaginaAbajo_" + (i + 1)).click(function (evento) {
            if (filtro.hasOwnProperty('filter'))
              filtrar({}, evento.currentTarget.id.split("_")[1], $("#ordenarSelect").val());
            else
              filtrar(evento.currentTarget.id.split("_")[1], $("#ordenarSelect").val());
          })

          $("#cambiarPagina_" + (i + 1)).click(function (evento) {
            if (filtro.hasOwnProperty('filter'))
              filtrar({}, evento.currentTarget.id.split("_")[1], $("#ordenarSelect").val());
            else
              filtrar(evento.currentTarget.id.split("_")[1], $("#ordenarSelect").val());
          });
        }


        everlive.Users.getById(item.Id).then(
          function (data) {
            usuario = data.result;
            if (usuario.hasOwnProperty('Picture')) {
              everlive.files.getDownloadUrlById(usuario.Picture).then(
                function (data) {
                  console.log("La imagen es:");
                  console.log(data);
                  var resultado = new ResultadoProfesor({
                    nombreProfesor: usuario.DisplayName,
                    descripcionProfesor: item.Descripcion,
                    calificacion: item.Puntaje,
                    precio: item.Precio,
                    clasesIndividuales: item.ClasesPersonal,
                    clasesGrupales: item.ClasesGrupo,
                    horarioDia: item.HorarioDia,
                    horarioTarde: item.HorarioTarde,
                    horarioNoche: item.HorarioNoche,
                    vaADomicilio: item.VaADomicilio,
                    usuarioProfesor: usuario.Username,
                    imagen: data
                  });
                  $("#resultados_profesores").append(resultado.bloqueResultado);
                });
            } else {
              var resultado = new ResultadoProfesor({
                nombreProfesor: data.result.DisplayName,
                descripcionProfesor: item.Descripcion,
                calificacion: item.Puntaje,
                precio: item.Precio,
                clasesIndividuales: item.ClasesPersonal,
                clasesGrupales: item.ClasesGrupo,
                horarioDia: item.HorarioDia,
                horarioTarde: item.HorarioTarde,
                horarioNoche: item.HorarioNoche,
                vaADomicilio: item.VaADomicilio,
                usuarioProfesor: data.result.Username
              });
              $("#resultados_profesores").append(resultado.bloqueResultado);
            }
          },
          function (error) {

          }
        );
      });
    },
    function (error) {}
  );
}
