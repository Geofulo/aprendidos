/*jslint plusplus: true */
/*globals console*/
var Cabecera = function (parametros) {
  "use strict";
  var contador, ciudad, materia;
  this.html =
    "<header class='general'>" +
    "<div class='contenedor-interno'>" +
    "<div class='col-lg-12'>" +
    parametros.navegacion.html +
    "</div>" +
    "</div>" +
    "<div id='informacionUsuarioActual'>" +
    "</div>" +
    "<div class='contenedor-interno'>" +
    "<div class='row titulo'>" +
    "quiero aprender" +
    "</div>" +
    "<div class='row subtitulo'>" +
    "busca a tu profesor" +
    "</div>" +
    "</div>" +
    "<form>" +
    "<div class='contenedor-interno'>" +
    "<section>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<label>Elige tu ciudad</label>" +
    "<select id='quieroAprenderCiudadSelect'>" +
    "<option></option>";

  for (contador = 0; contador < parametros.ciudades.length; contador++) {
    ciudad = parametros.ciudades[contador];
    this.html += "<option" + ((ciudad.Id === parametros.ciudadBuscada) ? ' selected ' : ' ') + "value='" + ciudad.Id + "'>" + ciudad.Nombre + "</option>";
  }

  this.html +=
    "</select>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='filtroPuntaje'>" +
    parametros.puntaje.bloque +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='filtroClases'>" +
    parametros.clases.bloque +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<label>Escribe la materia</label>" +
    "<select id='quieroAprenderMateriaSelect'>" +
    "<option></option>";

  for (contador = 0; contador < parametros.materias.length; contador++) {
    materia = parametros.materias[contador];
    this.html += "<option" + ((materia.Id === parametros.materiaBuscada) ? ' selected ' : ' ') + "value='" + materia.Id + "'>" + materia.Nombre + "</option>";
  }
  this.html +=
    "</select>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='filtroPrecioMaximo'>" +
    parametros.precio.bloque +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='filtroHorario'>" +
    parametros.horario.bloque +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<label>Ordenar</label>" +
    "<select id='ordenarSelect'>" +
    "<option></option>" +
    "<option value='Puntaje'>Puntaje</option>" +
    "<option value='Precio'>Precio</option>" +
    "</select>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='filtroGrado'>" +
    parametros.grado.bloque +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='filtroVaADomicilio'>" +
    parametros.vaADomicilio.bloque +
    "</div>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='derecha'>" +
    "<input type='button' class='boton' value='limpiar filtros' id='limpiarFiltrosBtn'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</header>";
};
