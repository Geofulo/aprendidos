var Resultados = function () {
  this.html = "<div class='contenedor-interno'>" +
    "<div class='row cantidad-resultados'>" +
    "<div class='col-lg-6'>" +
    "<h1>" +
    "Resultados <span id='cantidadResultados'></span>" +
    "</h1>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div class='derecha'>" +
    "<div class='opcion-paginacion pagina-anterior'></div>" +
    "<div id='contenidoEnlacesPaginas'></div>" +
    "<div class='opcion-paginacion pagina-siguiente'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div id='resultados_profesores'>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='derecha'>" +
    "<div class='opcion-paginacion pagina-anterior'></div>" +
    "<div id='contenidoEnlacesPaginasAbajo'></div>" +
    "<div class='opcion-paginacion pagina-siguiente'></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";

}
