var Cuerpo = function () {
  "use strict";
  this.html =
    "<div class='contenedor-interno'>" +
    "<form id='formuilarioEditarProfesor'>" +
    "<div class='row fila-con-espacio'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='submit' value='guardar cambios' class='boton-chico' id='guardarCambiosPerfilProfesor'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-3'>" +
    "<div id='foto_perfil' class='foto perfil'>" +
    "<a href='#' id='cambiar_foto'>Cambiar foto</a>" +
    "<form id='formularioImagenUsuario' method='post' enctype='multipart/form-data'>" +
    "<input type='file' id='foto_field' name='foto_field'>" +
    "</form>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div class='row'>" +
    "<input type='text' id='nombreProfesorPerfil' class='obscuro ajustable' placeholder='Nombre'>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='descripcion_biografia' id='descripcionBiografiaProfesor'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-3'>" +
    "<span id='gradoProfesorPerfil'>" +
    "</span>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span id='claseIndividualProfesorPerfil'></span>" +
    "<span id='claseGrupalProfesorPerfil'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span id='horarioDiaProfesorPerfil'></span>" +
    "<span id='horarioTardeProfesorPerfil'></span>" +
    "<span id='horarioNocheProfesorPerfil'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-3'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<span id='vaADomicilioProfesorPerfil' class='derecha'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    //"</div>" +
    //"</div>" +
    "<div class='col-lg-3'>" +
    "<div id='calificacionProfesorPerfil'></div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<span class='icono-precio-profesor-resultado'></span>" +
    "<span class='precio-profesor-resultado' id='cantidadPrecio'></span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +

    "<div class='row'>" +
    "<div class='col-lg-6 col-lg-offset-3 seccion-contacto-perfil'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 subtitulo'>" +
    "Contacto" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-6'>" +
    "<input type='email' class='claro ajustable email-field' id='emailProfesorPerfil' placeholder='Correo'>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<input type='text' class='claro ajustable phone-field' id='telefonoProfesorPerfil' placeholder='Teléfono'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-1 twitter-icon-field'>" +
    "</div>" +
    "<div class='col-lg-11'>" +
    "<input type='text' class='claro-ajustable' id='twitterProfesorPerfil' placeholder='Twitter'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-1 facebook-icon-field'></div>" +
    "<div class='col-lg-11'>" +
    "<input type='text' class='claro-ajustable' id='facebookProfesorPerfil' placeholder='Facebook'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-1 linkedin-icon-field'></div>" +
    "<div class='col-lg-11'>" +
    "<input type='text' class='claro-ajustable' id='linkedinProfesorPerfil' placeholder='LinkedIn'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<section>" +
    "<div class='titulo'>Sobre mi</div>" +
    "</section>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<textarea class='obscuro' id='sobreMiProfesorPerfil'></textarea>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<section>" +
    "<div class='titulo'>" +
    "Filtros" +
    "</div>" +
    "</section>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<section class='col-lg-12 sobresaliente-cb'>" +
    "<div class='row'>" +
    "<div class='col-lg-5'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<label>Elige tu Ciudad</label>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<select id='ciudadesSelect'>" +
    "<option></option>" +
    "</select>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-7'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<label>Escribe tu dirección</label>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<input type='text' class='claro-ajustable' id='direccionProfesorPerfil'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<div id='precioMaximoSlide'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='tipoGrupoSelect'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='vaADomicilioSelect'></div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4'>" +
    "<div id='gradoSelect'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='horariosSelect'></div>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<div id='limpiarFiltrosContenedor'>" +
    "<div id='limpiarFiltros'>" +
    "<a href='#' class='enlace' id='limpiarFiltrosEnlace'>Limpiar filtros</a>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "</div>" +
    "<div class='row'>" +
    "<section class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo'>" +
    "Materias" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='listaMateriasProfesor'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<select id='selectMateria'>" +
    "</select>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<a href='#' id='agregarMateriaLink' class='enlace'>agregar materia</a>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-derecha'>" +
    "<input type='button' class='chico' value='Sugerir materia' id='btn_abrirModalSugerirMateria'>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<section class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo'>" +
    "Últimos alumnos" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='listaUltimosAlumnos'>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-derecha'>" +
    "<a href='#' class='mas-resultados'>ver más</a>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>";
};
