/*globals $,everlive,Menu,Navegacion,CabeceraGeneral,Cuerpo,console*/
everlive.Users.currentUser().then(
  function (usuario) {
    "use strict";
    var self = this;
    var user = usuario.result;
    var menu = null,
      navegacion = null,
      cabecera = null,
      cuerpo = null,
      pie = null,
      menuSesion = null;
    console.log("El usuario incial es");
    console.log(user);
    if (user !== null) {
      if (user.userType === 'profesor') {
        console.log("El usuario");
        console.log(user);
        everlive.data('Profesor').getById(user.Id).then(
          function (profesor) {
            var profesor = profesor.result,
              modalSugerirMateria = new ModalSugerirMateria();

            console.log("El usuario es");
            console.log(user);
            menuSesion = new MenuSesion(profesor);
            menu = new Menu("quieroEnsenar", true, menuSesion);
            navegacion = new Navegacion();
            navegacion.setMenu(menu);
            cabecera = new CabeceraGeneral({
              navegacion: navegacion,
              titulo: 'Mi perfil',
              subtitulo: 'Profesor'
            });
            cuerpo = new Cuerpo();
            pie = new Pie();

            $("body").append(cabecera.html);
            $("body").append(cuerpo.html);
            $("body").append(pie.html);
            $("#dialogos").append(modalSugerirMateria.html);

            modalSugerirMateria.activarEventos();

            $("#btn_abrirModalSugerirMateria").click(function (event) {
              $("#modalSugerirMateria").show();
            });

            menuSesion.activarEventos();
            $("#foto_field").css('opacity', '0');

            $("#cambiar_foto").click(function (event) {
              event.preventDefault();
              $("#foto_field").trigger('click');
            });

            $("#guardarCambiosPerfilProfesor").click(function (event) {
              event.preventDefault();
              var ciudades = [];
              if (profesor.hasOwnProperty('Ciudades') && Array.isArray(profesor.Ciudades)) {
                ciudades = ciudades.concat(profesor.Ciudades);
              }

              if ($("#ciudadesSelect option:selected").val() !== '')
                ciudades = ciudades.concat($("#ciudadesSelect option:selected").val());

              var datosProfesor = everlive.data('Profesor'),
                valoresProfesorAGuardar = {
                  Id: profesor.Id,
                  Telefono: $("#telefonoProfesorPerfil").val(),
                  TwitterUrl: $("#twitterProfesorPerfil").val(),
                  FacebookUrl: $("#facebookProfesorPerfil").val(),
                  LinkedinUrl: $("#linkedinProfesorPerfil").val(),
                  Descripcion: $("#sobreMiProfesorPerfil").val(),
                  Direccion: $("#direccionProfesorPerfil").val(),
                  Precio: Number($("#precioMaximoField").val()),
                  ClasesGrupo: ($("#claseGrupalField").val() === '1') ? true : false,
                  ClasesPersonal: ($("#claseIndividualField").val() === '1') ? true : false,
                  VaADomicilio: ($("#vaADomicilioField").val() === '1') ? true : false,
                  Grado: $("#gradoSelectField").val(),
                  HorarioDia: ($("#horarioDiaField").val() === '1') ? true : false,
                  HorarioTarde: ($("#horarioTardeField").val() === '1') ? true : false,
                  HorarioNoche: ($("#horarioNocheField").val() === '1') ? true : false,
                  Ciudades: ciudades
                };

              datosProfesor.updateSingle(
                valoresProfesorAGuardar,
                function (profesorGuardado) {
                  console.log("Se guardo el profesor");
                  console.log(profesorGuardado);
                },
                function (error) {
                  console.log("No se guardo el profesor ");
                  console.log(error);
                }
              );


              var valoresUsuarioAGuardar = {
                Id: profesor.Id,
                DisplayName: $("#nombreProfesorPerfil").val(),
                Email: $("#emailProfesorPerfil").val()
              }


              everlive.Users.updateSingle(
                valoresUsuarioAGuardar,
                function (data) {
                  console.log("Se guardo el usuario");
                  console.log(data);
                  window.location.reload();
                },
                function (error) {
                  console.log("No se guardo el usuario");
                  console.log(error);
                  window.location.reload();
                });
            });


            everlive.data('Materia').get().then(
              function (data) {
                $.each(data.result, function (i, item) {
                  if (!item.hasOwnProperty('Sugerida') || item.Sugerida == false) {
                    if (!profesor.hasOwnProperty('Materias') || profesor.Materias.indexOf(item.Id) === -1) {
                      $("#selectMateria").append($('<option>', {
                        value: item.Id,
                        text: item.Nombre
                      }));
                    }
                  }
                });
                $("#selectMateria").select2();

              },
              function (error) {

              }
            );

            var sliderPrecio = new SlidePrecio('Un precio', 50, 500, 50, 4);
            $("#precioMaximoSlide").html(sliderPrecio.bloque);
            sliderPrecio.iniciar();
            sliderPrecio.activarEventos();

            sliderPrecio.getSlider().on('slidechange', function (event, ui) {
              $("#cantidadPrecio").text(ui.value);
            });

            var tipoGrupoSelect = new TipoGrupoSelect('Clases');
            $("#tipoGrupoSelect").html(tipoGrupoSelect.bloque);
            tipoGrupoSelect.activarEventos();

            tipoGrupoSelect.getGrupal().on('grupalesChange', function (event) {
              if (tipoGrupoSelect.isGrupal()) {
                $("#claseGrupalProfesorPerfil").removeClass();
                $("#claseGrupalProfesorPerfil").addClass('grupal-activado-profesor-resultado')
              } else {
                $("#claseGrupalProfesorPerfil").removeClass();
                $("#claseGrupalProfesorPerfil").addClass('grupal-desactivado-profesor-resultado')
              }
            });

            tipoGrupoSelect.getInidividual().on('individualesChange', function (event) {
              if (tipoGrupoSelect.isIndividual()) {
                $("#claseIndividualProfesorPerfil").removeClass();
                $("#claseIndividualProfesorPerfil").addClass('individual-activado-profesor-resultado')
              } else {
                $("#claseIndividualProfesorPerfil").removeClass();
                $("#claseIndividualProfesorPerfil").addClass('individual-desactivado-profesor-resultado')
              }
            });

            var vaADomicilioSelect = new VaADomicilioSelect(false);
            $("#vaADomicilioSelect").html(vaADomicilioSelect.bloque);
            vaADomicilioSelect.activarEventos();

            vaADomicilioSelect.getSelect().on('change', function (event) {
              if (vaADomicilioSelect.isVaADomicilio()) {
                $("#vaADomicilioProfesorPerfil").removeClass();
                $("#vaADomicilioProfesorPerfil").addClass('derecha va-a-domicilio-activado');
              } else {
                $("#vaADomicilioProfesorPerfil").removeClass();
                $("#vaADomicilioProfesorPerfil").addClass('derecha va-a-domicilio-desactivado');
              }
            })

            var gradoSelect = new GradoSelect(0);
            $("#gradoSelect").html(gradoSelect.bloque);
            gradoSelect.iniciar();
            gradoSelect.activarEventos();

            var horarioSelect = new HorarioSelect();
            $("#horariosSelect").html(horarioSelect.bloque);
            horarioSelect.activarEventos();

            $("#sobreMiProfesorPerfil").on('keyup', function (event) {
              $("#descripcionBiografiaProfesor").text($("#sobreMiProfesorPerfil").val());
            });

            horarioSelect.getHorarioDia().on('horarioDiaChange', function (event) {
              if (horarioSelect.isHorarioDia()) {
                $("#horarioDiaProfesorPerfil").removeClass();
                $("#horarioDiaProfesorPerfil").addClass('horario-dia-activado-profesor-resultado');
              } else {
                $("#horarioDiaProfesorPerfil").removeClass();
                $("#horarioDiaProfesorPerfil").addClass('horario-dia-desactivado-profesor-resultado');
              }
            });

            horarioSelect.getHorarioTarde().on('horarioTardeChange', function (event) {
              if (horarioSelect.isHorarioTarde()) {
                $("#horarioTardeProfesorPerfil").removeClass();
                $("#horarioTardeProfesorPerfil").addClass('horario-tarde-activado-profesor-resultado');
              } else {
                $("#horarioTardeProfesorPerfil").removeClass();
                $("#horarioTardeProfesorPerfil").addClass('horario-tarde-desactivado-profesor-resultado');
              }
            });

            horarioSelect.getHorarioNoche().on('horarioNocheChange', function (event) {
              if (horarioSelect.isHorarioNoche()) {
                $("#horarioNocheProfesorPerfil").removeClass();
                $("#horarioNocheProfesorPerfil").addClass('horario-noche-activado-profesor-resultado');
              } else {
                $("#horarioNocheProfesorPerfil").removeClass();
                $("#horarioNocheProfesorPerfil").addClass('horario-noche-desactivado-profesor-resultado');
              }
            });

            var calificacion = new Calificacion(0);
            calificacion.iniciar();
            $("#calificacionProfesorPerfil").html(calificacion.bloque);

            $("#limpiarFiltrosEnlace").click(function (event) {
              event.preventDefault();
              sliderPrecio.setPrecio(0);
              tipoGrupoSelect.desactivarClasesGrupales();
              tipoGrupoSelect.desactivarClasesIndividuales();
              vaADomicilioSelect.desactivarVaADomicilio();
              gradoSelect.setGrado(0);
              horarioSelect.desactivarHorarioDia();
              horarioSelect.desactivarHorarioTarde();
              horarioSelect.desactivarHorarioNoche();
            });

            console.log("OItro punto del usuario");
            console.log(user);

            $("#nombreProfesorPerfil").val(user.DisplayName);
            $("#emailProfesorPerfil").val(user.Email);
            $("#telefonoProfesorPerfil").val(profesor.Telefono);
            $("#twitterProfesorPerfil").val(profesor.TwitterUrl);
            $("#facebookProfesorPerfil").val(profesor.FacebookUrl);
            $("#linkedinProfesorPerfil").val(profesor.LinkedinUrl);
            $("#sobreMiProfesorPerfil").val(profesor.Descripcion);
            $("#descripcionBiografiaProfesor").text(profesor.Descripcion);

            $("#direccionProfesorPerfil").val(profesor.Direccion);

            $("#agregarMateriaLink").click(function (event) {
              event.preventDefault();
              if (profesor.hasOwnProperty('Materias')) {
                var materias = profesor.Materias;
              } else {
                var materias = [];
              }

              materias.push($("#selectMateria").val());
              event.preventDefault();
              everlive.data('Profesor').updateSingle({
                Id: profesor.Id,
                Materias: materias
              }, function (data) {
                console.log("Se agrego la materia");
                window.location.reload();
              }, function (error) {
                console.log("no se agrego la materia");
              })

            });


            everlive.data('Ciudad').get().then(
              function (data) {
                console.log("Si entro a las ciudades");
                console.log(data);
                $.each(data.result, function (i, item) {
                  $("#ciudadesSelect").append($("<option>", {
                    value: item.Id,
                    text: item.Nombre
                  }));
                });
              },
              function (error) {

              }
            );

            $("#ciudadesSelect").select2({
              language: {
                noResults: function () {
                  return "No se encontró ninguna ciudad";
                }
              }
            });

            sliderPrecio.setPrecio(profesor.Precio);
            $("#cantidadPrecio").text(profesor.Precio);

            if (profesor.ClasesGrupo) {
              $("#claseGrupalProfesorPerfil").addClass('grupal-activado-profesor-resultado');
              tipoGrupoSelect.activarClasesGrupales();
            } else {
              $("#claseGrupalProfesorPerfil").addClass('grupal-desactivado-profesor-resultado');
              tipoGrupoSelect.desactivarClasesGrupales();
            }

            if (profesor.ClasesPersonal) {
              tipoGrupoSelect.activarClasesIndividuales();
              $("#claseIndividualProfesorPerfil").addClass('individual-activado-profesor-resultado');
            } else {
              tipoGrupoSelect.desactivarClasesIndividuales();
              $("#claseIndividualProfesorPerfil").addClass('individual-desactivado-profesor-resultado');
            }

            if (profesor.VaADomicilio) {
              vaADomicilioSelect.activarVaADomicilio();
              $("#vaADomicilioProfesorPerfil").addClass('va-a-domicilio-activado');
            } else {
              vaADomicilioSelect.desactivarVaADomicilio();
              $("#vaADomicilioProfesorPerfil").addClass('va-a-domicilio-desactivado');
            }

            gradoSelect.setGrado(profesor.Grado);
            calificacion.setCalificacion(profesor.Puntaje);
            $("#calificacionProfesorPerfil").html(calificacion.bloque);

            if (profesor.HorarioDia) {
              horarioSelect.activarHorarioDia();
              $("#horarioDiaProfesorPerfil").addClass('horario-dia-activado-profesor-resultado');
            } else {
              horarioSelect.desactivarHorarioDia();
              $("#horarioDiaProfesorPerfil").addClass('horario-dia-desactivado-profesor-resultado');
            }

            if (profesor.HorarioTarde) {
              horarioSelect.activarHorarioTarde();
              $("#horarioTardeProfesorPerfil").addClass('horario-tarde-activado-profesor-resultado');
            } else {
              horarioSelect.desactivarHorarioTarde();
              $("#horarioTardeProfesorPerfil").addClass('horario-tarde-desactivado-profesor-resultado');
            }

            if (profesor.HorarioNoche) {
              horarioSelect.activarHorarioNoche();
              $("#horarioNocheProfesorPerfil").addClass('horario-noche-activado-profesor-resultado');
            } else {
              horarioSelect.desactivarHorarioNoche();
              $("#horarioNocheProfesorPerfil").addClass('horario-noche-desactivado-profesor-resultado');
            }

            $.each(profesor.Materias, function (i, item) {
              everlive.data('Materia').getById(item).then(
                function (data) {
                  var grados = ['Ninguno', 'Universidad', 'Maestria', 'Doctorado'];
                  var materiaItem = new MateriaItem(data.result.Nombre, grados[profesor.Grado], data.result.Id, true);
                  $("#listaMateriasProfesor").append(materiaItem.bloque);
                  $("#eliminar_" + data.result.Id).click(function (event) {
                    var materias = profesor.Materias;
                    var eliminar = materias.indexOf(data.result.Id);
                    console.log("El indice es:");
                    console.log(eliminar);
                    materias.splice(eliminar, 1);
                    console.log(materias);
                    everlive.data('Profesor').updateSingle({
                        Id: profesor.Id,
                        Materias: materias
                      },
                      function (data) {
                        window.location.reload();
                      },
                      function (error) {
                        console.log("Error al eliminar la materia");
                      }
                    );
                  });
                },
                function (error) {}
              )
            });
          });
      } else {
        var modalConfirmarProfesor = new ModalConfirmarProfesor(user);
        $("#dialogos").append(modalConfirmarProfesor.html);
        modalConfirmarProfesor.activarEventos();
        $("#modalConfirmacionProfesor").show();
      }
    } else {
      var modalInicioSesion = new ModalInicioSesion(),
        modalRegistro = new ModalRegistro();
      $("#dialogos").append(modalInicioSesion.html);
      $("#dialogos").append(modalRegistro.html);
      modalInicioSesion.activarEventos();
      modalRegistro.activarEventos();
      $("#modalInicioSesion").show();
    }
  }
);
