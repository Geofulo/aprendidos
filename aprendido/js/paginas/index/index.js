/*global $,everlive, Menu, Navegacion, Pie, ModalInicioSesion, ModalRegistro, CabeceraIndex, CuerpoIndex, MenuSesion,console*/

everlive.Users.currentUser()
  .then(function (usuario) {
    "use strict";
    var ordenarCiudad = new Everlive.Query();
    ordenarCiudad.order('Prioridad');
    everlive.data('Ciudad').get(ordenarCiudad).then(
      function (ciudades) {
        var ordenarMateria = new Everlive.Query();
        ordenarMateria.order('Nombre');
        everlive.data('Materia').get(ordenarMateria).then(
          function (materias) {
            var menu = null,
              navegacion = new Navegacion(),
              pie = new Pie(),
              modalInicioSesion = new ModalInicioSesion(),
              modalRegistro = new ModalRegistro(),
              menuSesion = null,
              cabeceraIndex = new CabeceraIndex({
                listaMaterias: materias,
                listaCiudades: ciudades
              }),
              cuerpoIndex = new CuerpoIndex();

            if (usuario.result !== null) {
              menuSesion = new MenuSesion(usuario.result);
              menu = new Menu('quieroAprender', true, menuSesion);
            } else {
              menu = new Menu('quieroAprender', false);
            }

            navegacion.setMenu(menu);
            cabeceraIndex.setNavegacion(navegacion);
            $("body")
              .prepend(cabeceraIndex.html);
            $("body")
              .append(pie.html);
            $("main")
              .append(cuerpoIndex.html);
            $("#dialogos")
              .append(modalInicioSesion.html)
              .append(modalRegistro.html);

            menu.activarEventos();
            modalInicioSesion.activarEventos();
            modalRegistro.activarEventos();
            menuSesion.activarEventos();


          }
        );
      }
    );
  });
