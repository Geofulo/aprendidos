/*global Aprendido */
var CuerpoIndex = function () {
  "use strict";
  this.html =
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<h1 class='titulo'>cómo funciona</h1>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-4 contenido-centrado'>" +
    "<img src='" + Aprendido.raiz + "/imgs/iconos/busca_profe.svg' width='90' />" +
    "<div class='subtitulo'>" +
    "Busca Profe" +
    "</div>" +
    "<div class='texto'>" +
    "Encontrá tu profe ideal. Podés elegir la distancia entre sí, un precio adecuado y ver su reputación" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4 contenido-centrado'>" +
    "<img src='" + Aprendido.raiz + "/imgs/iconos/toma_tu_clase.svg' width='90' />" +
    "<div class='subtitulo'>" +
    "Toma tu clase" +
    "</div>" +
    "<div class='texto'>" +
    "Pedís tu clase y ambos van a poder contactarse de una manera mas cómoda" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-4 contenido-centrado'>" +
    "<img src='" + Aprendido.raiz + "/imgs/iconos/aprendido.svg' width='90' />" +
    "<div class='subtitulo'>" +
    "¡Aprendiendo!" +
    "</div>" +
    "<div class='texto'>" +
    "Lo más importante, la meta de todos. Ahora aprender es más simple" +
    "</div>" +
    "</div>" +
    "</div>";
};
