/*jslint plusplus: true */
/*global console*/
var CabeceraIndex = function (parametros) {
  "use strict";
  var contador, materia, ciudad;

  this.html = "<header class='index'>";
  this.setNavegacion = function (navegacion) {
    this.html +=
      navegacion.html +
      "<div class='row'>" +
      "<div class='col-lg-12 titulo contenido-centrado'>" +
      "Aprender es más simple" +
      "</div>" +
      "<div class='row'>" +
      "<div class='col-lg-8 col-lg-offset-2 subtitulo contenido-centrado'>" +
      "Encontrar un profesor nunca fue tan fácil." +
      "</div>" +
      "</div>" +
      "<div class='col-lg-12'>" +
      "<div id='cuadroBuscarProfesor'>" +
      "<div class='titulo contenido-centrado'>" +
      "Busca a tu profesor" +
      "</div>" +
      "<div id='formulario_buscar_profesor'>" +
      "<form method='get' action='quiero_aprender.html'>" +
      "<div class='row contenido-centrado'>" +
      "<select name='ciudadSelect' id='ciudadSelect'>" +
      "<option></option>";

    for (contador = 0; contador < parametros.listaCiudades.count; contador++) {
      ciudad = parametros.listaCiudades.result[contador];
      this.html += "<option value='" + ciudad.Id + "'>" + ciudad.Nombre + "</option>";
    }

    this.html +=
      "</select>" +
      "</div>" +
      "<div class='row contenido-centrado'>" +
      "<select name='materiaSelect' id='materiaSelect'>" +
      "<option></option>";

    for (contador = 0; contador < parametros.listaMaterias.count; contador++) {
      if (!parametros.listaMaterias.result[contador].hasOwnProperty('Sugerida') || parametros.listaMaterias.result[contador].Sugerida === false) {
        materia = parametros.listaMaterias.result[contador];
        this.html += "<option value='" + materia.Id + "'>" + materia.Nombre + "</option>";
      }
    }

    this.html +=
      "</select>" +
      "</div>" +
      "<div class='row'>" +
      "<div class='col-lg-12 contenido-centrado'>" +
      "<input type='submit' value='Buscar' class='boton semiajustable' id='buscarProfesorSubmit'>" +
      "</div>" +
      "</div>" +
      "</form>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</header>";
  };
};
