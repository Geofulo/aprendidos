everlive.Users.currentUser().then(
  function (usuario) {
    var menu = null,
      navegacion = null,
      cabecera = null,
      cuerpo = null,
      pie = null,
      menuSesion = null,
      modalInicioSesion = new ModalInicioSesion(),
      modalRegistro = new ModalRegistro();

    if (usuario.result !== null) {
      menuSesion = new MenuSesion(usuario.result);
      menu = new Menu('somos', true, menuSesion);
    } else {
      menu = new Menu('somos', false);
      $("#dialogos").append(modalInicioSesion.html);
      $("#dialogos").append(modalRegistro.html);
      modalInicioSesion.activarEventos();
      modalRegistro.activarEventos();
    }

    navegacion = new Navegacion();
    navegacion.setMenu(menu);
    cabecera = new CabeceraGeneral({
      navegacion: navegacion,
      titulo: '',
      subtitulo: ''
    });
    cuerpo = new Cuerpo();

    pie = new Pie();

    $("body").append(cabecera.html);
    $("body").append(cuerpo.html);
    $("body").append(pie.html);


    menu.activarEventos();
    menuSesion.activarEventos();
  });
