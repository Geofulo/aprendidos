var Cuerpo = function () {
  this.html =
    "<div class = 'contenedor-interno' >" +
    "<ul class='nav nav-tabs'>" +
    "<li class='active'><a data-toggle='tab' href='#somos'>Nosotros somos</a></li>" +
    "<li><a data-toggle='tab' href='#preguntasFrecuentes'>Preguntas Frecuentes</a></li>" +
    "<li><a data-toggle='tab' href='#contacto'>Contacto</a></li>" +
    "</ul>" +
    "<div class='tab-content'>" +
    "<div id='somos' class='tab-pane fade in active contenidoTab'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "Hola, mi nombre es Alex y tengo 21 años. Soy al que se le ocurrió el nombre \"Aprendidos\". La idea del sitio no sé de quien es, o de hecho creo que es de todos y es un deseo a la vez. El deseo de que la educación llegue de la mejor manera a quien lo solicite, en cualquier momento y lugar. Esa es nuestra meta y formas parte de ella, ya dimos el primer paso y falta mucho por recorrer." +
    "¿Qué quiénes somos? Aprendidos somos todos ¡Crezcamos juntos!" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div id='preguntasFrecuentes' class='tab-pane fade contenidoTab'>" +
    "<div class='row'>" +
    "<a data-toggle='collapse' href='#respuesta_3' aria-expanded='false' onClick='cambiarIcono(3)'>" +
    "<div class='col-lg-1'>" +
    "<div class='icono-mas derecha' id='mostrar_pregunta_3'></div>" +
    "</div>" +
    "<div class='col-lg-11 contenido-en-medio pregunta_frecuente'>" +
    "¿Cómo busco un profesor particular?" +
    "</div>" +
    "</a>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='collapse col-lg-11 col-lg-offset-1 respuesta' id='respuesta_3'>" +
    "Es muy sencillo. Seleccionas el lugar donde vivís y la materia que necesitas." +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<a data-toggle='collapse' href='#respuesta_4' aria-expanded='false' onClick='cambiarIcono(4)'>" +
    "<div class='col-lg-1'>" +
    "<div class='icono-mas derecha' id='mostrar_pregunta_4'></div>" +
    "</div>" +
    "<div class='col-lg-11 contenido-en-medio pregunta_frecuente'>" +
    "¿Cómo encuentro el profe ideal para mí?" +
    "</div>" +
    "</a>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='collapse col-lg-11 col-lg-offset-1 respuesta' id='respuesta_4'>" +
    "Tenemos herramientas que te van a ayudar. Por ejemplo podes elegir en que horario preferís la clase, o si la vas a tener en tu domicilio o no, entre otras cosas. Además podes ver y elegir a partir de la reputación del profe." +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<a data-toggle='collapse' href='#respuesta_5' aria-expanded='false' onClick='cambiarIcono(5)'>" +
    "<div class='col-lg-1'>" +
    "<div class='icono-mas derecha' id='mostrar_pregunta_5'></div>" +
    "</div>" +
    "<div class='col-lg-11 contenido-en-medio pregunta_frecuente'>" +
    "¿Por qué debería tener una cuenta en Aprendidos?" +
    "</div>" +
    "</a>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='collapse col-lg-11 col-lg-offset-1 respuesta' id='respuesta_5'>" +
    "Creemos que el futuro ya llegó, y estar en internet se volvió vital. Registrarse es la mejor forma de publicitarse y llegar más lejos. ¡Estamos en toda Argentina!" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<a data-toggle='collapse' href='#respuesta_6' aria-expanded='false' onClick='cambiarIcono(6)'>" +
    "<div class='col-lg-1'>" +
    "<div class='icono-mas derecha' id='mostrar_pregunta_6'></div>" +
    "</div>" +
    "<div class='col-lg-11 contenido-en-medio pregunta_frecuente'>" +
    "¿Cuál es el costo de registrarme?" +
    "</div>" +
    "</a>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='collapse col-lg-11 col-lg-offset-1 respuesta' id='respuesta_6'>" +
    "El costo de pertenecer a Aprendidos puede definirse como un compromiso con los estudiantes. La meta es aprender, y para eso necesitamos enseñar. Más allá de esto, no hay costo alguno." +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div id='contacto' class='tab-pane fade contenidoTab'>" +
    "<div class='row'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo-formulario titulo-dudas'>" +
    "Envianos tus dudas" +
    "</div>" +
    "</div>" +
    "<div class='row tabla'>" +
    "<form>" +
    "<div class='col-lg-6 celda-tabla'>" +
    "<label for='duda_nombre'>Nombre</label>" +
    "<input type='text' class='obscuro' name='duda_nombre'>" +
    "<label for='duda_email'>E-mail*</label>" +
    "<input type='text' class='obscuro' name='duda_email'>" +
    "<label for='duda_telefono'>Teléfono</label>" +
    "<input type='text' class='obscuro' name='duda_telefono'>" +
    "</div>" +
    "<div class='col-lg-6 celda-tabla'>" +
    "<label>Mensaje</label>" +
    "<textarea class='obscuro'></textarea>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-6 col-lg-offset-6 contenido-derecha'>" +
    "<input type='button' class='boton-chico' id='btn_enviarPregunta' value='enviar'>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>" +
    "</div>";
}

function cambiarIcono(identificadorPregunta) {
  $("#respuesta_" + identificadorPregunta).on('hide.bs.collapse', function () {
    $("#mostrar_pregunta_" + identificadorPregunta).css("background-image", "url('" + Aprendido.raiz + "/imgs/iconos/ic_mas.svg')");
  });

  $("#respuesta_" + identificadorPregunta).on('show.bs.collapse', function () {
    $("#mostrar_pregunta_" + identificadorPregunta).css("background-image", "url('" + Aprendido.raiz + "/imgs/iconos/ic_menos.svg')");
  });
}
