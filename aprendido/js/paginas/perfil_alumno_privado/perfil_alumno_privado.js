/*globals everlive,Menu,Navegacion,Cuerpo,$,CabeceraGeneral,Pie,console*/
everlive.Users.currentUser().then(
  function (data) {
    "use strict";
    var menu = null,
      navegacion = null,
      cabecera = null,
      cuerpo = null,
      pie = null,
      menuSesion = null;
    if (data.result === null || data.result.userType !== 'alumno') {
      $("#modalInicioSesion").show();
    } else {
      menuSesion = new MenuSesion(data.result);
      menu = new Menu('', true, menuSesion);
      navegacion = new Navegacion();
      navegacion.setMenu(menu);
      cabecera = new CabeceraGeneral({
        navegacion: navegacion,
        titulo: 'Mi perfil',
        subtitulo: 'Alumno'
      });
      cuerpo = new Cuerpo();
      pie = new Pie();

      $("body").append(cabecera.html);
      $("body").append(cuerpo.html);
      $("body").append(pie.html);
      menuSesion.activarEventos();

      //$("#formularioImagenUsuario").attr('action', everlive.files.getUploadUrl());
      //alert($("#formularioImagenUsuario").attr('action'));
      $("#foto_field").css('opacity', '0');
      $("#cambiar_foto").click(function (event) {
        event.preventDefault();
        $("#foto_field").trigger('click');
      });
      console.log("La url para la imagen es:");
      console.log(everlive.files.getUploadUrl());

      $("#foto_field").on('change', function (even) {
        var nuevaFoto = document.getElementById("foto_field").files;
        var formData = new FormData();
        var archivo = nuevaFoto[0];
        console.log(archivo.name);
        formData.append('imagen_usuario', archivo, archivo.name);
        var enviarTelerik = new XMLHttpRequest();
        enviarTelerik.open('POST', everlive.files.getUploadUrl(), true);
        enviarTelerik.onload = function () {
          if (enviarTelerik.status === 200) {
            var respuestaJSON = JSON.parse(enviarTelerik.response);
            var usuario = data.result;
            console.log("sjdkafasdklf el usauro");
            console.log(usuario);
            if (data.result.hasOwnProperty('Picture')) {
              everlive.Files.destroySingle({
                  Id: data.result.Picture
                },
                function () {
                  everlive.Users.updateSingle({
                    Id: usuario.Id,
                    'Picture': respuestaJSON.Result[0].Id
                  }, function (data) {
                    window.location.reload();
                  }, function (error) {
                    console.log("Error");
                    console.log(error);
                  });
                },
                function (error) {
                  console.log("Error al eliminar la imagen anterior");
                  console.log(error);
                })
            } else {
              everlive.Users.updateSingle({
                Id: usuario.Id,
                'Picture': respuestaJSON.Result[0].Id
              }, function (data) {
                window.location.reload();
              }, function (error) {
                console.log("Error");
                console.log(error);
              });
            }
          } else {}
        };
        enviarTelerik.send(formData);
      });

      if (data.result.hasOwnProperty('Picture')) {
        everlive.files.getDownloadUrlById(data.result.Picture).then(
          function (data) {
            $("#foto_perfil").css('background-image', 'url(' + data + ')');
            $("#foto_perfil").css('background-size', 'cover');
            $("#foto_perfil").css('background-position', 'center');
          },
          function (error) {
            console.log("Error al obtener la imagen");
            console.log(error);
          }
        );
      }

      $("#formuilarioEditarAlumno").submit(function (event) {
        event.preventDefault();
        var datos = {
          Id: data.result.Id,
          DisplayName: $("#nombreAlumnoPerfil").val(),
          Email: $("#emailAlumnoPerfil").val(),
          Phone: $("#telefonoAlumnoPerfil").val()
        }

        console.log(datos);

        everlive.Users.updateSingle(
          datos,
          function (data) {
            console.log("Alumno actualizado");
            window.location.reload();
          },
          function (error) {
            console.log("Alumno no actualizado");
            console.log(error);
          });
      });

      everlive.data('Clase').get({
        Alumno: data.result.Id
      }).then(
        function (data) {
          console.log("Las clases que se obtuvieron son:");
          console.log(data);
          $.each(data.result, function (index, item) {

            var clase = item;
            var query = new Everlive.Query();
            query.where().isin('Clases', clase.Id);
            everlive.data('Profesor').get({
              Clases: item.Id
            }).then(
              function (data) {
                console.log("El profesor que se obtuvo según la clase es");
                console.log(data);
                $.each(data.result, function (index, item) {
                  everlive.Users.getById(item.Id).then(
                    function (data) {
                      everlive.data('Materia').getById(clase.Materia).then(function (materia) {
                        console.log("Las materias que se obtuvieron son");
                        console.log(materia);
                        var materiaItem = new MateriaItem(materia.result.Nombre, Aprendido.grados[item.Grado], false);
                        $("#listaMateriasAlumno").append(materiaItem.bloque);
                      })
                      console.log("obtenida otra");
                      console.log(data);
                      if (data.result.hasOwnProperty('Picture')) {
                        var profesor = data.result;
                        everlive.files.getDownloadUrlById(data.result.Picture).then(
                          function (data) {
                            if (!clase.hasOwnProperty('Puntuacion') || !clase.hasOwnProperty('Comentario')) {
                              var itemProfesorCalificar = new UltimosProfesoresCalificarItem(clase.Id, data, profesor.DisplayName, everlive, item.Id);
                              $("#listaUltimosProfesores").append(itemProfesorCalificar.bloque);
                              itemProfesorCalificar.activarEventos();
                            } else {
                              var itemProfesor = new UltimosProfesoresItem(item.Id, data, profesor.DisplayName, clase.Puntuacion, clase.Comentario);
                              $("#listaUltimosProfesores").append(itemProfesor.bloque);
                            }
                          },
                          function (error) {
                            console.log("Error al obtener la imagen");
                            console.log(error);
                          }
                        );
                      } else {
                        if (!clase.hasOwnProperty('Puntuacion') || !clase.hasOwnProperty('Comentario')) {
                          var itemProfesorCalificar = new UltimosProfesoresCalificarItem(clase.Id, false, data.result.DisplayName, everlive, item.Id);
                          $("#listaUltimosProfesores").append(itemProfesorCalificar.bloque);
                          itemProfesorCalificar.activarEventos();
                        } else {
                          var itemProfesor = new UltimosProfesoresItem(item.Id, false, data.result.DisplayName, clase.Puntuacion, clase.Comentario);
                          $("#listaUltimosProfesores").append(itemProfesor.bloque);
                        }
                      }
                    }
                  );
                });
              },
              function (error) {
                console.log("Error al obtener las clases en el profesor");
                console.log(error);
              }
            );
            //var ultimoProfesorItem = new UltimosProfesoresItem(id_profesor, foto, nombre, calificacion, acercaDe);
            //$("#listaUltimosProfesores").append(index);
          });
        },
        function (error) {
          console.log("Error al cargar las clases");
          console.log(error);
        }
      );

      $("#nombreAlumnoPerfil").val(data.result.DisplayName);
      $("#emailAlumnoPerfil").val(data.result.Email);
      $("#telefonoAlumnoPerfil").val(data.result.Phone);

    }
  },
  function (error) {
    window.location.replace("/aprendido");
  });
