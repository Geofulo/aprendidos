var Cuerpo = function () {
  "use strict";
  this.html = "<div class='contenedor-interno perfil'>" +
    "<form id='formuilarioEditarAlumno'>" +
    "<div class='row fila-con-espacio'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='submit' value='guardar cambios' class='boton chico'+ id='guardarCambiosPerfilAlumno'>" +
    "</div>" +
    "</div>" +
    "<section>" +
    "<div class='titulo'>Datos generales</div>" +
    "<div class='row'>" +
    "<div class='col-lg-3'>" +
    "<div id='foto_perfil' class='foto perfil'>" +
    "<a href='#' id='cambiar_foto'>Cambiar foto</a>" +
    "<form id='formularioImagenUsuario' method='post' enctype='multipart/form-data'>" +
    "<input type='file' id='foto_field' name='foto_field'>" +
    "</form>" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-9'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<label>Nombre completo</label>" +
    "<input type='text' id='nombreAlumnoPerfil' class='obscuro ajustable'+ placeholder='Nombre'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-9 col-lg-offset-3 seccion-contacto-perfil'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 subtitulo'>" +
    "Contacto" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-5'>" +
    "<input type='email' class='claro ajustable email-field'+ id='emailAlumnoPerfil' placeholder='Correo'>" +
    "</div>" +
    "<div class='col-lg-7'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<input type='text' class='claro ajustable phone-field'+ id='telefonoAlumnoPerfil' placeholder='Teléfono'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<section class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo'>" +
    "Materias" +
    "</div>" +
    "</div>" +
    "<div class='row' id='listaMateriasAlumno'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<section class='col-lg-6'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo'>" +
    "Últimos profesores" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12' id='listaUltimosProfesores'>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-derecha'>" +
    "<a href='#' class='mas-resultados'>ver más</a>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</section>" +
    "</form>" +
    "</div>";
};
