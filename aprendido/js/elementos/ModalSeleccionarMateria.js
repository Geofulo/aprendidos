var ModalSeleccionarMateria = function () {
  this.html =
    "<div id='modalSeleccionMateria' class='modal'>" +
    "<div class='content'>" +
    "<div class='header'>" +
    "<div id='closeModalSeleccionMateria' class='closeModal'>x</div>" +
    "</div>" +
    "<div class='body'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo-modal-agregar-materia contenido-centrado'>" +
    "Materia a tomar" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 lista-materias-modal'>" +
    "<form id='formularioSeleccionMateria'>" +
    "</form>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='footer'>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.activarEventos = function () {
    $("#closeModalSeleccionMateria").click(function (event) {
      $("#modalSeleccionMateria").hide();
    });
  }
}
