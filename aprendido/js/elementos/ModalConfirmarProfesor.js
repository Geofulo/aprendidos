var ModalConfirmarProfesor = function (usuario) {
  'use strict';
  var self = this;
  var usuario = usuario;
  this.html =
    "<div id='modalConfirmacionProfesor' class='modal'>" +
    "<div class='content'>" +
    "<div class='header'>" +
    "<div id='cloaseModalConfirmacionProfesor' class='closeModal'>x</div>" +
    "</div>" +
    "<div class='body'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo-modal-confirmacionProfesor contenido-centrado'>" +
    "Ser profesor" +
    "</div>" +
    "</div>" +
    "<div class='row fila-con-espacio contenido-centrado'>" +
    "<div class='col-lg-12'>" +
    "Al entrar a esta sección, te convertirás en profesor" +
    "</div>" +
    "</div>" +
    "<form id='formularioConfirmacionProfesor'>" +
    "<div class='row'>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='submit' class='chico' value='ser profesor' id='confirmarSerProfesor'>" +
    "</div>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='submit' class='chico' value='cancelar' id='cancelarSerProfesor'>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>" +
    "<div class='footer'>" +
    "</div>" +
    "</div>" +
    "</div>";


  this.activarEventos = function () {
    $("#modalConfirmacionProfesor").click(function (event) {
      $("#modalConfirmacionProfesor").hide();
    });
    $("#confirmarSerProfesor").click(function (event) {
      event.preventDefault();
      everlive.data('Profesor').create({
        Id: usuario.Id,
        DisplayName: usuario.DisplayName,
        HorarioDia: false,
        HorarioTarde: false,
        HorarioNoche: false,
        VaADomicilio: false,
        Precio: 0,
        Puntaje: 0,
        ClasesPersonal: false,
        Grado: 0,
        Descripcion: "-"
      }, function (data) {
        everlive.Users.updateSingle({
          Id: data.result.Id,
          userType: 'profesor'
        }, function (data) {
          window.location.reload();
        })
      });
    });
    $("#cancelarSerProfesor").click(function (event) {
      event.preventDefault();
      window.location.replace(Aprendido.raiz);
    });
  };
};
