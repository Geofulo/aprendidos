var ModalTipoUsuario = function () {
  "use strict";
  this.html =
    "<div id='modalTipoUsuario' class='modal'>" +
    "<div class='content'>" +
    "<div class='header'>" +
    "</div>" +
    "<div class='body'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<h1>Tipo usuario</h1>" +
    "</div>" +
    "</div>" +
    "<form id='formularioTipoUsuario'>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "Hola <span id='usuarioRedSocial'></span>, estás registrado mediante <span id='redSocialRegistrada'></span> es necesario que indiques tu tipo de usuario." +
    "</div>" +
    "</div>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='radio' name='userType' value='profesor'> Profesor" +
    "</div>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='radio' name='userType' value='alumno'> Alumno" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='submit' name='saveUserType' class='boton' id='saveUserType' value='Guardar'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<a href='#' class='enlace' id='cerrarSesion'>Cerrar sesión</a>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>" +
    "<div class='footer'>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.activarEventos = function () {};
};
