/*global Aprendido*/
var Pie = function () {
  "use strict";
  var elementosMenu = {
      quieroAprender: {
        enlace: 'quiero_aprender.html',
        texto: 'Quiero Aprender'
      },
      quieroEnsenar: {
        enlace: 'quiero_ensenar.html',
        texto: 'Quiero Enseñar'
      },
      preguntasFrecuentes: {
        enlace: 'preguntas_frecuentes.html',
        texto: 'Preguntas Frecuentes'
      }
    },
    elementoMenu;

  this.html =
    "<footer>" +
    "<div class='contenedor-interno'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<img src='" + Aprendido.raiz + "/imgs/footer_logo.jpg'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-1'>" +
    "<menu>" +
    "<ul>" +
    "<li>" +
    "<a href='#'>REGISTRO</a>" +
    "</li>" +
    "</ul>" +
    "</menu>" +
    "</div>" +
    "<div class='col-lg-10 contenido-centrado'>" +
    "<menu>" +
    "<ul>";

  for (elementoMenu in elementosMenu) {
    if (elementosMenu.hasOwnProperty(elementoMenu)) {
      this.html += "<li><a href='" + elementosMenu[elementoMenu].enlace + "'>" + elementosMenu[elementoMenu].texto + "</a></li>";
    }
  }

  this.html +=
    "</ul>" +
    "</menu>" +
    "</div>" +
    "<div class='col-lg-1'>" +
    "&nbsp;" +
    "<p>" +
    "<div class='row' id='redesSociales'>" +
    "<div class='col-lg-4' id='twitter'>" +
    "</div>" +
    "<div class='col-lg-4' id='facebook'>" +
    "</div>" +
    "<div class='col-lg-4' id='linkedin'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</footer>";
};
