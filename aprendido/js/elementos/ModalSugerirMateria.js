var ModalSugerirMateria = function () {
  this.html =
    "<div id='modalSugerirMateria' class='modal'>" +
    "<div class='content'>" +
    "<div class='header'>" +
    "<div id='closeModalSugerirMateria' class='closeModal'>x</div>" +
    "</div>" +
    "<div class='body'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 titulo-modal-sugerir-materia contenido-centrado'>" +
    "Sugerir Materia" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<form id='formularioSugerirMateria'>" +
    "<input placeholder='Escribe el nombre de la materia a sugerir' type='text' class='obscuro ajustable' name='materiaSugerida' id='materiaSugerida'/>" +
    "<input type='button' class='chico' value='sugerir materia' id='btn_sugerirMateria'/>" +
    "</form>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='footer'>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.activarEventos = function () {
    $("#closeModalSugerirMateria").click(function () {
      $("#modalSugerirMateria").hide();
    });

    $("#btn_sugerirMateria").click(function () {
      everlive.data('Materia').create({
        Nombre: $("#materiaSugerida").val(),
        Sugerida: true
      }, function (materia) {
        $("#modalSugerirMateria").hide();
      })
    });
  }
}
