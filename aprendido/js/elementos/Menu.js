/*global $, Aprendido*/
var Menu = function (paginaActual, sesionActiva, menuSesion) {
  "use strict";
  this.sesionActiva = sesionActiva;

  var self = this,
    elementosMenu = {
      quieroAprender: {
        texto: 'quiero aprender',
        enlace: Aprendido.raiz + '/quiero_aprender.html'
      },
      quieroEnsenar: {
        texto: 'quiero enseñar',
        enlace: Aprendido.raiz + '/perfil_profesor_privado.html'
      },
      somos: {
        texto: 'somos',
        enlace: Aprendido.raiz + '/somos.html'
      }
    },
    elemento;

  if (!this.sesionActiva) {
    elementosMenu.login = {
      texto: 'iniciar sesion',
      enlace: '#',
      id: 'iniciarSesionLink'
    };
  }

  this.html = "<menu>" +
    "<ul>";

  for (elemento in elementosMenu) {
    if (elementosMenu.hasOwnProperty(elemento)) {
      if (elementosMenu[elemento].hasOwnProperty('id')) {
        this.html += "<li><a id='" + elementosMenu[elemento].id + "' href='" + elementosMenu[elemento].enlace + "'>" + elementosMenu[elemento].texto + "</a></li>";
      } else {
        if (elemento === paginaActual) {
          this.html += "<li class='seleccionado'>" + elementosMenu[elemento].texto + "</li>";
        } else {
          this.html += "<li><a href='" + elementosMenu[elemento].enlace + "'>" + elementosMenu[elemento].texto + "</a></li>";
        }
      }
    }
  }


  if (this.sesionActiva) {
    this.html += menuSesion.html;
  }
  this.html += "</ul>";

  this.html += "</menu>";
  this.activarEventos = function () {
    $("#iniciarSesionLink")
      .click(function () {
        $('#modalRegistro')
          .css('display', 'none');
        $('#modalInicioSesion')
          .css('display', 'block');
      });
  };

  this.setIsSesionActiva = function (isSesionActiva) {
    self.sesionActiva = isSesionActiva;
  };
};
