/*global Aprendido*/
var Navegacion = function () {
  "use strict";
  var self = this;

  this.html =
    "<div class='contenedor-interno'>" +
    "<nav class='row'>" +
    "<div class='col-lg-3'>" +
    "<a href='" + Aprendido.raiz + "'>" +
    "<img src='" + Aprendido.raiz + "/imgs/menu_logo.png'>" +
    "</a>" +
    "</div>" +
    "<div class='col-lg-9'>";

  this.setMenu = function (menu) {
    self.html +=
      menu.html +
      "</div>" +
      "</nav>" +
      "</div>";
  };
};
