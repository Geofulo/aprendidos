var MenuSesion = function (usuario) {
  "use strict";
  var self = this;
  var user = usuario;
  this.html =
    "<div id='informacionSesion'>" +
    "<div id='zonaNotificacion'>" +
    "</div>" +
    "<div id='zonaInformacionUsuario'>" +
    "<div id='fotoYNombre'>" +
    "<div id='fotoUsuario'>" +
    "</div>" +
    "<div id='nombreUsuario'>" +
    usuario.DisplayName.split(" ")[0] +
    "</div>" +
    "<div id='flechaDespliegueInfoSesion' class='flecha-muestra'>" +
    "</div>" +
    "</div>" +
    "<div id='opcionMiPerfil'>" +
    "<a href='perfil_" + usuario.userType + "_privado.html'>Mi perfil</a>" +
    "</div>" +
    "<div id='opcionCerrarSesion'>" +
    "<a>Cerrar sesión</a>" +
    "</div>" +
    "</div>" +
    "</div>";

  this.finalizarSesion = function () {
    everlive.authentication.logout(
      function () {
        window.location.reload();
      });
  }

  this.activarEventos = function () {
    if (user.hasOwnProperty('Picture'))
      everlive.files.getDownloadUrlById(user.Picture).then(
        function (data) {
          $("#fotoUsuario").css('background-image', 'url(' + data + ')');
          $("#fotoUsuario").css('background-size', 'cover');
          $("#fotoUsuario").css('background-position', 'center');
        },
        function (error) {
          console.log("Error al obtener la imagen");
          console.log(error);
        }
      );
    $("#flechaDespliegueInfoSesion").click(function (event) {
      if ($("#zonaInformacionUsuario").css('background-color') == 'rgba(0, 0, 0, 0)') {
        $("#zonaInformacionUsuario").css('background-color', '#ebebeb');
        $("#zonaInformacionUsuario").css('-webkit-box-shadow', '0px 0px 11px -1px rgba(0,0,0,0.3)');
        $("#zonaInformacionUsuario").css('-moz-box-shadow', '0px 0px 11px -1px rgba(0,0,0,0.3)');
        $("#zonaInformacionUsuario").css('box-shadow', '0px 0px 11px -1px rgba(0,0,0,0.3)');
        $("#flechaDespliegueInfoSesion").removeClass();
        $("#flechaDespliegueInfoSesion").addClass('flecha-oculta');
        $("#opcionCerrarSesion").show();
        $("#opcionMiPerfil").show();
        $("#opcionCerrarSesion").click(self.finalizarSesion);
        $("#zonaNotificacion").css('border-right', '2px solid transparent');
      } else {
        $("#zonaInformacionUsuario").css('background-color', 'transparent');
        $("#opcionCerrarSesion").hide()
        $("#opcionMiPerfil").hide();
        $("#flechaDespliegueInfoSesion").removeClass();
        $("#flechaDespliegueInfoSesion").addClass('flecha-muestra');
        $("#zonaInformacionUsuario").css('-webkit-box-shadow', 'none');
        $("#zonaInformacionUsuario").css('-moz-box-shadow', 'none');
        $("#zonaInformacionUsuario").css('box-shadow', 'none');
        $("#zonaNotificacion").css('border-right', '2px solid darkgray');
      }
    });

    if (!user.hasOwnProperty('userType')) {
      everlive.data('Profesor').getById(user.Id).then(
        function (data) {
          var profesor = data.result;
          if (data.result.hasOwnProperty('Clases') && (data.result.Clases).length > 0) {
            console.log("LAs clases");
            console.log(data);
            $.each(data.result.Clases, function (index, item) {
              if (!Array.isArray(item))
                everlive.data('Clase').getById(item).then(
                  function (data) {
                    console.log("un resultado de la clase es");
                    console.log(data.result);
                    clase = data.result;
                    if (!clase.hasOwnProperty('isNotificacionPendiente') || clase.isNotificacionPendiente) {
                      everlive.data('Materia').getById(clase.Materia).then(
                        function (data) {
                          var materia = data.result;
                          everlive.Users.getById(clase.Alumno).then(
                            function (data) {
                              alumno = data.result;
                              console.log("Notificacion");
                              var tiempoNotificacion = data.result.CreatedAt;
                              var tiempoActual = new Date();
                              var diferenciaDias = tiempoNotificacion.getDate() - tiempoActual.getDate();
                              var diferenciaMeses = tiempoNotificacion.getMonth() - tiempoActual.getMonth();
                              var diferenciaAnios = tiempoNotificacion.getFullYear() - tiempoActual.getFullYear();
                              var diferenciaHoras = tiempoNotificacion.getHours() - tiempoActual.getHours();
                              var diferenciaMinutos = tiempoNotificacion.getMinutes() - tiempoActual.getMinutes();
                              var mensajeTiempo = "";

                              if (diferenciaAnios > 0) {
                                mensajeTiempo = "Hace " + diferenciaAnios + " año" + ((diferenciaAnios > 1) ? 's' : '');
                              } else if (diferenciaMeses > 0) {
                                mensajeTiempo = "Hace " + diferenciaMeses + " mes" + ((diferenciaMeses > 1) ? 'es' : '');
                              } else if (diferenciaDias > 0) {
                                mensajeTiempo = "Hace " + diferenciaDias + " dia" + ((diferenciaDias > 1) ? 's' : '');
                              } else if (diferenciaHoras > 0) {
                                mensajeTiempo = "Hace " + diferenciaHoras + " hora" + ((diferenciaHoras > 1) ? 's' : '');
                              } else if (diferenciaMinutos > 0) {
                                mensajeTiempo = "Hace " + diferenciaMinutos + " minuto" + ((diferenciaMinutos > 1) ? 's' : '');
                              } else {
                                mensajeTiempo = "Ahora";
                              }
                              var listaItemNotificaciones = [];
                              var itemNotificaciones = new NotificacionesItem(clase.Id, alumno.DisplayName, "Solicitud para tomar clase de " + materia.Nombre, mensajeTiempo, (((profesor.Clases).length - 1) == index) ? true : false);
                              listaItemNotificaciones.push(itemNotificaciones);

                              var notificaciones = new Notificaciones();
                              $("body").append(notificaciones.bloque);
                              notificaciones.acomodarBloque();
                              notificaciones.insertarNotificaciones(listaItemNotificaciones);
                              console.log(alumno.DisplayName + " solicito la clase de la materia " + materia.Nombre + " " + mensajeTiempo);
                              itemNotificaciones.activarEventos();
                            }
                          )
                        }
                      );
                    }
                  });
            })
          }
        }
      );
      $("#zonaNotificacion").click(function (event) {
        $("#numero-notificaciones").fadeOut('fast');
        if ($("#notificaciones").css('display') == 'none')
          $("#notificaciones").fadeIn("fast");
        else
          $("#notificaciones").fadeOut("fast");
      });
    }
  };
};
