/*jslint browser: true*/
/*global $, everlive, console */
var ModalInicioSesion = function () {
  "use strict";
  var self = this;
  this.html =
    "<div id='modalInicioSesion' class='modal'>" +
    "<form id='formularioInicioSesion' autocomplete='off'>" +
    "<div class='content'>" +
    "<div class='header'>" +
    "<div id='closeModalSesion' class='closeModal'>x</div>" +
    "</div>" +
    "<div class='body'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<h1>Iniciar Sesión</h1>" +
    "</div>" +
    "</div>" +
    //"<div class='row redes-sociales'>" +
    //"<div class='col-lg-6'>" +
    //"<div class='boton facebook derecha contenido-centrado' id='inicioSesionFacebook'>" +
    //"<div class='icono facebook separacion-blanca-vertical-derecha'></div>" +
    //"con facebook" +
    //"</div>" +
    //"</div>" +
    //"<div class='col-lg-6'>" +
    //"<div class='boton google izquierda contenido-centrado' id='inicioSesionGooglePlus'>" +
    //"<div class='icono google separacion-blanca-vertical-derecha'></div>" +
    //"con Google Plus" +
    //"</div>" +
    //"</div>" +
    //"</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado contenido-en-medio'>" +
    "<input type='email' class='obscuro' name='login_username' id='login_username' placeholder='Correo electrónico'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado contenido-en-medio'>" +
    "<input type='password' class='obscuro' name='login_password' id='login_password' placeholder='Contraseña'>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div class='error' id='error_inicio_sesion'></div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='submit' class='boton-chico' id='btn_iniciarSesion' value='iniciar sesión'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='footer'>" +
    "<div class='row'>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "¿No te has registrado aún? <a href='#' id='registrateLink'>Regístrate</a>" +
    "</div>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "¿Olvidaste tu contraseña? <a href='#'>Haz click aquí.</a>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>";

  this.iniciarSesionPorFormulario = function () {
    //if ($("#formularioInicioSesion").valid()) {
    everlive.authentication.login(
      $("#login_username").val(),
      $("#login_password").val(),
      function (data) {
        everlive.Users.currentUser().then(
          function (data) {
            if (data.result.userType === "alumno") {
              $("#modalInicioSesion").hide();
              window.location.reload();
            } else if (data.result.userType === "profesor") {
              $("#modalInicioSesion").hide();
              window.location.replace("/aprendido/perfil_profesor_privado.html");
            }
          },
          function (error) {
            console.log("Error al autenticar:");
            console.log(error);
          }
        );
      },
      function (error) {
        console.log(error);
        if (error.code === 205) {
          $("#error_inicio_sesion").text("Nombre de usuario o contraseña inválidos");
        }
      }
    );
    //}
  };

  this.activarEventos = function () {
    $("#closeModalSesion").click(function (event) {
      $("#modalInicioSesion").hide();
    });

    $("#registrateLink")
      .click(function () {
        $('#modalInicioSesion').hide();
        $('#modalRegistro').show();
      });

    $("#btn_iniciarSesion")
      .click(function (event) {
        event.preventDefault();
        self.iniciarSesionPorFormulario();
      });
  };
};
