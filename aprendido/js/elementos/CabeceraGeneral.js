var CabeceraGeneral = function (parametros) {
  "use strict";
  this.html =
    "<header class='general'>" +
    "<div class='contenedor-interno'>" +
    "<div class='col-lg-12'>" +
    parametros.navegacion.html +
    "</div>" +
    "</div>" +
    "</nav>" +
    "</div>" +
    "</div>" +
    "<div id='informacionUsuarioActual'>" +
    "</div>" +
    "<div class='contenedor-interno'>" +
    "<div class='row titulo'>" +
    parametros.titulo +
    "</div>" +
    "<div class='row subtitulo'>" +
    parametros.subtitulo +
    "</div>" +
    "</div>" +
    "</header>";
};
