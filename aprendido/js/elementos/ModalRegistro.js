/*jslint browser: true, plusplus: true */
/*global $, alert, everlive,console */
var ModalRegistro = function () {
  "use strict";
  var meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    contador,
    anioActual = (new Date()).getFullYear(),
    self = this;

  this.html =
    "<div id='modalRegistro' class='modal modal-registro'>" +
    "<form id='formularioRegistro'>" +
    "<div class='content'>" +
    "<div class='header'>" +
    "<div id='closeModalRegistro' class='closeModal'>x</div>" +
    "</div>" +
    "<div class='body'>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<h1>Registro</h1>" +
    "</div>" +
    "<div id='errorRegistroUsuarioPorFormulario' class='error'></div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12'>" +
    "<div id='error-tipoUsuario'></div>" +
    "</div>" +
    "</div>" +
    "<div class='row tipo-registro'>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='radio' name='tipoUsuario' id='tipoUsuario_alumno' value='alumno'>" +
    "<label for='tipoUsuario_alumno'>Quiero aprender</label>" +
    "</div>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='radio' name='tipoUsuario' id='tipoUsuario_profesor' value='profesor'>" +
    "<label for='tipoUsuario_profesor'>Quiero enseñar</label>" +
    "</div>" +
    "</div>" +
    /*"<div class='row redes-sociales'>" +
    "<div class='col-lg-6'>" +
    "<div class='boton facebook derecha contenido-centrado'>" +
    "<div class='icono facebook separacion-blanca-vertical-derecha'></div>" +
    "con facebook" +
    "</div>" +
    "</div>" +
    "<div class='col-lg-6'>" +
    "<div class='boton google izquierda contenido-centrado'>" +
    "<div class='icono google separacion-blanca-vertical-derecha'></div>" +
    "con Google Plus" +
    "</div>" +
    "</div>" +
    "</div>" +*/
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='email' class='obscuro' id='register_username' name='username' placeholder='Correo electrónico' required title='Campo de correo requerido'>" +
    "<div class='error' id='error-username'></div>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<h2>Fecha de nacimiento</h2>" +
    "</div>" +
    "</div>" +
    "<div class='row fecha-nacimiento'>" +
    "<div class='col-lg-4'>" +
    "<select class='obscuro' id='selectDiaNacimiento'>";

  for (contador = 1; contador <= 31; contador++) {
    this.html += "<option value='" + contador + "'>" + contador + "</option>";
  }

  this.html +=
    "</select>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<select class='obscuro' id='selectMesNacimiento'>";

  for (contador = 0; contador < meses.length; contador++) {
    this.html += "<option value='" + (contador + 1) + "'>" + meses[contador] + "</option>";
  }

  this.html +=
    "</select>" +
    "</div>" +
    "<div class='col-lg-4'>" +
    "<select class='obscuro' id='selectAnioNacimiento'>";


  for (contador = anioActual - 13; contador >= anioActual - 100; contador--) {
    this.html += "<option value='" + contador + "'>" + contador + "</option>";
  }

  this.html +=
    "</select>" +
    "</div>" +
    "</div>" +
    "<div class='row'>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='password' class='obscuro' id='register_password' name='password' placeholder='Contraseña'>" +
    "</div>" +
    "<div class='col-lg-12 contenido-centrado'>" +
    "<input type='password' class='obscuro' name='register_confirmPassword' placeholder='Confirmar contraseña'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "<div class='footer'>" +
    "<div class='row'>" +
    "<div class='col-lg-6 contenido-centrado yaTengoCuenta'>" +
    "<a href='#' id='iniciarSesionLink-modal' class='enlace'>Ya tengo cuenta</a>" +
    "</div>" +
    "<div class='col-lg-6 contenido-centrado'>" +
    "<input type='submit' class='chico' value='Registrarme' id='btn_registrarUsuario'>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</form>" +
    "</div>";

  this.registrarUsuarioPorFormulario = function () {
    console.log($("#register_username").get()[0].checkValidity());
    var datosUsuario = {
      userType: $('input[name="tipoUsuario"]:checked').val(),
      DisplayName: $("#register_username").val(),
      Email: $("#register_username").val(),
      AnioNacimiento: $("#selectAnioNacimiento option:selected").val(),
      MesNacimiento: $("#selectMesNacimiento option:selected").val(),
      DiaNacimiento: $("#selectDiaNacimiento option:selected").val()
    };

    everlive.Users.register(
      $("#register_username").val(),
      $("#register_password").val(),
      datosUsuario,
      function (data) {
        if (datosUsuario.userType === 'profesor') {
          everlive.data('Profesor').create({
            Id: data.result.Id,
            HorarioDia: false,
            HorarioTarde: false,
            HorarioNoche: false,
            VaADomicilio: false,
            Precio: 0,
            Puntaje: 0,
            ClasesPersonal: false,
            Grado: 0,
            Descripcion: "-",
            DisplayName: $("#register_username").val(),
          }, function (data) {
            $('#modalRegistro').css('display', 'none');
            $('#modalInicioSesion').css('display', 'block');
          }, function (errorProfesor) {
            $("#errorRegistroUsuarioPorFormulario").text("Error al registrar usuario. Favor de intentarlo de nuevo.");
          });
        } else {
          $('#modalRegistro').css('display', 'none');
          $('#modalInicioSesion').css('display', 'block');
        }
      },
      function (error) {
        if (error.code === 201) {
          $("#error-username").text("El usuario ya existe, favor de elegir otro nombre de usuario.");
        } else {
          $("#errorRegistroUsuarioPorFormulario").text("Error al registrar usuario. Favor de intentarlo de nuevo.");
          console.log(error);
        }
      }
    );

  };

  this.activarEventos = function () {
    $("#closeModalRegistro").click(function (event) {
      $("#modalRegistro").hide();
    });

    $('#btn_registrarUsuario')
      .click(function (event) {
        event.preventDefault();
        self.registrarUsuarioPorFormulario();
      });
    $("#iniciarSesionLink-modal")
      .click(function (event) {
        event.preventDefault();
        $("#modalRegistro").hide();
        $("#modalInicioSesion").show();
      });
  };
};
